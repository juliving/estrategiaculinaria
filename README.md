# Repositorio para el desarrollo del proyecto de *Estrategia culinaria* #
Aquí se encuentran alojados los artefactos de ingeniería y archivos del proyecto.
El repositorio para las entregas por equipo de las materias se encuentra [aquí](https://bitbucket.org/juliving/daw "aquí").


## Repositorio para entrega de actividades por equipo: ##
[AQUÍ](https://bitbucket.org/juliving/daw "AQUÍ")


## Miembros ##

* A01206568 Juan Manuel Ledesma Rangel
* A01700457 Lino Ronaldo Contreras Gallegos
* A01700762 Ivett Núñez Martínez
