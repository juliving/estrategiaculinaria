<?php

use Codeception\Util\Locator;

class InsumosCest
{
    public function _before(AcceptanceTester $I){
      $I->amOnPage('/login');
      $I->fillField('usuario','admin');
      $I->fillField('contrasenia','admin');
      $I->click('Iniciar Sesión');
    }

    public function _after(AcceptanceTester $I){
    }

    public function correctRegister(AcceptanceTester $I){
  		$I->wantTo('TC05E-A | Registrar insumo correctamente');
          $I->lookForwardTo('Registrar un insumo y ser redirigido a la sección insumos');
          $I->amOnPage('/insumos/registrar');
          $I->see('Registrar insumo');
          $I->fillField('nombre','Galletas');
          $I->fillField('descripcion','Galletas sabor chocolate con chispas de chocolate blanco');
          $I->fillField(Locator::option('kilogramos(s)') , '#unidad');
          $I->fillField('unidad','pz');
          $I->click('Registrar');
          $I->see('Proveedores que ofrecen insumo');
  	}

}
