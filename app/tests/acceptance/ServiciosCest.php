<?php

class ServiciosCest
{
  public function _before(AcceptanceTester $I){
    $I->amOnPage('/login');
    $I->fillField('usuario','admin');
    $I->fillField('contrasenia','admin');
    $I->click('Iniciar Sesión');
  }

  public function _after(AcceptanceTester $I){
  }

  public function listingExists(AcceptanceTester $I){
    $I->wantTo('TC06E-A | Lista de servicios desplegada desde BD');
    $I->lookForwardTo('Ver la lista de todos los servicios registrados en la base de datos');
    $I->amOnPage('/servicios/listar');
    $I->see('Nombre del servicio');
  }

  public function correctRegister(AcceptanceTester $I){
    $I->wantTo('TC06E-B | Registrar servicio correctamente');
    $I->lookForwardTo('Registrar un servicio y ser redirigido a la sección Servicios');
    $I->amOnPage('/servicios/registrar');
    $I->see('Registrar servicio');
    $I->fillField('nombre','Mariachi');
    $I->fillField('descripcion','Mariachi para eventos empresariales');
    $I->fillField('precio','6000');
    $I->click('Registrar');
    //$I->see('Bienvenido admin');
  }


}
