<?php


class CuentasCest
{
    public function _before(AcceptanceTester $I){
    }

    public function _after(AcceptanceTester $I){
    }

    public function correctLogin(AcceptanceTester $I){
        $I->wantTo('TC01E-A | Autenticarse correctamente');
        $I->lookForwardTo('Iniciar sesión y ser redirigido a la página de bienvenida');
        $I->amOnPage('/login');
        $I->see('Inicio de sesión');
        $I->fillField('usuario','admin');
        $I->fillField('contrasenia','admin');
        $I->click('Iniciar Sesión');
        $I->see('Bienvenido admin');
    }

    public function correctUserWrongPasswordLogin(AcceptanceTester $I){
        $I->wantTo('TC01E-B | Autenticarse contraseña incorrecta');
        $I->lookForwardTo('No obtener acceso al sistema y ser retroalimentado');
        $I->amOnPage('/login');
        $I->see('Inicio de sesión');
        $I->fillField('usuario','admin');
        $I->fillField('contrasenia','incorrect');
        $I->click('Iniciar Sesión');
        $I->see('Inicio de sesión');
        $I->seeInCurrentUrl('failed');
    }

    public function wrongUserCorrectPassword(AcceptanceTester $I){
        $I->wantTo('TC01E-C | Autenticarse usuario incorrecto contraseña correcta');
        $I->lookForwardTo('No obtener acceso al sistema y ser retroalimentado');
        $I->amOnPage('/login');
        $I->see('Inicio de sesión');
        $I->fillField('usuario','incorrect');
        $I->fillField('contrasenia','admin');
        $I->click('Iniciar Sesión');
        $I->see('Inicio de sesión');
        $I->seeInCurrentUrl('failed');
    }

    public function wrongUserWrongPassword(AcceptanceTester $I){
        $I->wantTo('TC01E-D | Autenticarse usuario incorrecto contraseña incorrecta');
        $I->lookForwardTo('No obtener acceso al sistema y ser retroalimentado');
        $I->amOnPage('/login');
        $I->see('Inicio de sesión');
        $I->fillField('usuario','incorrect');
        $I->fillField('contrasenia','incorrect');
        $I->click('Iniciar Sesión');
        $I->see('Inicio de sesión');
        $I->seeInCurrentUrl('failed');
    }

    public function tryAccessLoginRestrictedSection(AcceptanceTester $I)
    {
        $I->wantTo('be redirected to /login?url=/eventos');
        $I->amOnPage('/eventos');
        $I->seeCurrentUrlEquals('/login?url=/eventos');
        $I->see('Inicio de sesión');
    }

    public function showRecoveryForm(AcceptanceTester $I){
      $I->wantTo('TC01E-E | Mostrar formulario de recuperación');
      $I->lookForwardTo('Mostrar el formulario de recuperación de contraseñas');
      $I->amOnPage('/login');
      $I->see('Inicio de sesión');
      $I->click('Recuperar contraseña');
      $I->see('Recuperar contraseña');
      $I->seeInCurrentUrl('/cuentas/recuperar');
    }

    public function RetrieveWrongAdmin(AcceptanceTester $I){
      $I->wantTo('TC01E-F | Solicitar recuperación de contraseña con usuario inexistente');
      $I->lookForwardTo('Mostrar mensaje de usuario no registrado');
      $I->amOnPage('/cuentas/recuperar');
      $I->see('Recuperar contraseña');
      $I->fillField('usuario', 'knfdnflds');
      $I->click('Recuperar');
      $I->see('Recuperación de contraseña');
      $I->seeInCurrentUrl('/login?recoveryRequest=1');
    }

    public function RetrieveCorrectAdmin(AcceptanceTester $I){
      $I->wantTo('TC01E-G | Solicitar recuperación de contraseña con usuario existente');
      $I->lookForwardTo('Mostrar mensaje de envío de link al correo');
      $I->amOnPage('/cuentas/recuperar');
      $I->see('Recuperar contraseña');
      $I->fillField('usuario', 'linocontreras');
      $I->click('Recuperar');
      $I->see('Recuperación de contraseña');
      $I->seeInCurrentUrl('/login?recoveryRequest=1');
    }

    public function RecoverWrongAdmin(AcceptanceTester $I){
      $I->wantTo('TC01E-H | Recuperar contraseña con usuario inexistente');
      $I->lookForwardTo('Mostrar mensaje de usuario no registrado');
      $token = $I->grabColumnFromDatabase('cuenta', 'recuperar', array('id' => 'RebOOter'));
      $I->amOnPage('/cuentas/recuperando'.$token);
      $I->see('Recuperar contraseña');
      $I->fillField('usuario', 'knfdnflds');
      $I->fillField('contrasenia', 'knfdnflds');
      $I->click('Recuperar');
      $I->see('Usuario no encontrado');
      $I->seeInCurrentUrl('/login?recoveryRequest=1');
    }

    public function RecoverCorrectAdmin(AcceptanceTester $I){
      $I->wantTo('TC01E-I | Recuperar contraseña con usuario existente');
      $I->lookForwardTo('Mostrar mensaje de contraseña modificada');
      $token = $I->grabColumnFromDatabase('cuenta', 'recuperar', array('id' => 1));
      $I->amOnPage('/cuentas/recuperando'.$token);
      $I->see('Recuperar contraseña');
      $I->fillField('usuario', 'admin');
      $I->fillField('contrasenia', 'nuevoadmin');
      $I->click('Recuperar');
      $I->see('Contraseña reestablecida correctamenteo');
      $I->seeInCurrentUrl('/login?recovery=1');
    }
}
