<?php


class ProveedoresCest
{
    public function _before(AcceptanceTester $I){
      $I->amOnPage('/login');
      $I->fillField('usuario','admin');
      $I->fillField('contrasenia','admin');
      $I->click('Iniciar Sesión');
    }

    public function _after(AcceptanceTester $I){
    }

    public function details(AcceptanceTester $I){
      $I->wantTo('TC04E-A | Consultar información proveedor');
      $I->lookForwardTo('Obtener acceso a la información detallada del proveedor');
      $I->amOnPage('/proveedores/listar');
      $I->see('Nombre del proveedor');
      $I->click('Lino Contreras');
      $I->see('Teléfonos');
      $I->seeInCurrentUrl('/proveedores/detalles/2');
    }

    public function correctRegister(AcceptanceTester $I){
  		$I->wantTo('TC04E-B | Registrar proveedor correctamente');
          $I->lookForwardTo('Registrar un proveedor y ser redirigido a la sección Proveedores');
          $I->amOnPage('/personas/registrar?tipo=proveedor');
          $I->see('Registrar proveedor');
          $I->fillField('nombre','Juan Manuel Ledesma Rangel');
          $I->fillField('correo','A01206568@itesm.mx');
          $I->click('Registrar');
          $I->see('Datos personales');
  	}

}
