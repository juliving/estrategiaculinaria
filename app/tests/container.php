<?php

require __DIR__ . '/../vendor/autoload.php';

use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Container;

session_start();

$container = new Container([
    App::class => function (ContainerInterface $c) {
        $settings = require __DIR__ . '/../src/settings.php';
        $app = new \Slim\App($settings);
        require __DIR__ . '/../src/dependencies.php';
        require __DIR__ . '/../src/middleware.php';
        require __DIR__ . '/../src/routes.php';
        return $app;
    }
]);

return $container;
