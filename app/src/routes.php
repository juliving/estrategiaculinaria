<?php

use Juliving\EstrategiaCulinaria\Controllers as Controllers;
use Juliving\EstrategiaCulinaria\Middleware as Middleware;

// Routes

$app->get('/', Controllers\Home::class . ':home' )
    ->setName('/');

$app->get('/agenda', Controllers\Agenda::class . ':index')
    ->setName('/agenda');
$app->get('/agenda/sendevents', Controllers\Agenda::class . ':sendEvents')
    ->setName('/agenda/sendevents');

$app->get('/clientes', Controllers\Clientes::class . ':index' )
    ->setName('/clientes');
$app->get('/clientes/detalles/{id:[0-9]+}', Controllers\Clientes::class . ':detail')
    ->setName('/clientes/detalles');
$app->get('/clientes/listar[/{page:[0-9]+}]', Controllers\Clientes::class . ':list')
    ->setName('/clientes/listar');
$app->get('/clientes/buscar', Controllers\Clientes::class . ':search')
    ->setName('/clientes/buscar');
$app->get('/clientes/searchAjax', Controllers\Clientes::class . ':searchAjax')
    ->setName('/clientes/searchAjax');

$app->get('/empleados', Controllers\Empleados::class . ':index')
    ->setName('/empleados');
$app->get('/empleados/detalles/{id:[0-9]+}', Controllers\Empleados::class . ':detail')
    ->setName('/empleados/detalles');
$app->get('/empleados/listar[/{page:[0-9]+}]', Controllers\Empleados::class . ':list')
    ->setName('/empleados/listar');
$app->get('/empleados/buscar', Controllers\Empleados::class . ':search')
    ->setName('/empleados/buscar');
$app->get('/empleados/searchAjax', Controllers\Empleados::class . ':searchAjax' )
    ->setName('/empleados/searchAjax');

$app->get('/eventos', Controllers\Eventos::class . ':index' )
    ->setName('/eventos');
$app->get('/eventos/listar', Controllers\Eventos::class . ':list' )
    ->setName('/eventos/listar');
$app->get('/eventos/registrar', Controllers\Eventos::class . ':showAddForm' )
    ->setName('/eventos/registrar');
$app->post('/eventos/registrar', Controllers\Eventos::class . ':add' )
    ->setName('/eventos/registrar');
$app->get('/eventos/getEventTasksAjax/{id}', Controllers\Eventos::class . ':getEventTasksAjax')
    ->setName('/eventos/getEventTasksAjax');
$app->post('/eventos/postTareaState', Controllers\Eventos::class . ':postTareaState')
    ->setName('/eventos/postTareaState');

$app->get('/insumos', Controllers\Insumos::class . ':index' )
    ->setName('/insumos');
$app->get('/insumos/detalles/{id:[0-9]+}', Controllers\Insumos::class . ':detail')
    ->setName('/insumos/detalles');
$app->get('/insumos/listar[/{page:[0-9]+}]', Controllers\Insumos::class . ':list')
    ->setName('/insumos/listar');
$app->get('/insumos/registrar', Controllers\Insumos::class . ':showAddForm')
    ->setName('/insumos/registrar');
$app->post('/insumos/registrar', Controllers\Insumos::class . ':add')
    ->setName('/insumos/registrar');
$app->get('/insumos/buscar', Controllers\Insumos::class . ':search')
    ->setName('/insumos/buscar');
$app->get('/insumos/searchAjax', Controllers\Insumos::class . ':searchAjax')
    ->setName('/insumos/searchAjax');
$app->get('/insumos/editar/{id:[0-9]+}', Controllers\Insumos::class . ':showEditForm' )
    ->setName('/insumos/editar');
$app->post('/insumos/editar/{id:[0-9]+}', Controllers\Insumos::class . ':edit' )
    ->setName('/insumos/editar');
$app->get('/insumos/reportes', Controllers\Insumos::class . ':reporte' )
    ->setName('/insumos/reportes');
$app->get('/insumos/eliminar', Controllers\Insumos::class . ':erase')
    ->setName('/insumos/eliminar');

$app->get('/personas/registrar', Controllers\Personas::class . ':showAddForm')
    ->setName('/personas/registrar');
$app->post('/personas/registrar', Controllers\Personas::class . ':add' )
    ->setName('/personas/registrar');
$app->get('/personas/editar/{id:[0-9]+}', Controllers\Personas::class . ':showEditForm' )
    ->setName('/personas/editar');
$app->post('/personas/editar/{id:[0-9]+}', Controllers\Personas::class . ':edit' )
    ->setName('/personas/editar');
$app->get('/personas/eliminar', Controllers\Personas::class . ':erase')
    ->setName('/personas/eliminar');

$app->get('/proveedores', Controllers\Proveedores::class . ':index' )
    ->setName('/proveedores');
$app->get('/proveedores/detalles/{id:[0-9]+}', Controllers\Proveedores::class . ':detail' )
    ->setName('/proveedores/detalles');
$app->get('/proveedores/listar[/{page:[0-9]+}]', Controllers\Proveedores::class . ':list' )
    ->setName('/proveedores/listar');
$app->get('/proveedores/registrar', Controllers\Proveedores::class . ':showAddForm' )
    ->setName('/proveedores/registrar');
$app->post('/proveedores/registrar', Controllers\Proveedores::class . ':add' )
    ->setName('/proveedores/registrar');
$app->get('/proveedores/buscar', Controllers\Proveedores::class . ':search' )
    ->setName('/proveedores/buscar');
$app->get('/proveedores/searchAjax', Controllers\Proveedores::class . ':searchAjax' )
    ->setName('/proveedores/searchAjax');
$app->get('/proveedores/porServicioAjax', Controllers\Proveedores::class . ':porServicioAjax' )
    ->setName('/proveedores/porServicioAjax');
$app->get('/proveedores/eliminar', Controllers\Proveedores::class . ':erase')
    ->setName('/proveedores/eliminar');

$app->get('/servicios', Controllers\Servicios::class . ':index')
    ->setName('/servicios');
$app->get('/servicios/detalles/{id:[0-9]+}', Controllers\Servicios::class . ':detail')
    ->setName('/servicios/detalles');
$app->get('/servicios/listar[/{page:[0-9]+}]', Controllers\Servicios::class . ':list')
    ->setName('/servicios/listar');
$app->get('/servicios/registrar', Controllers\Servicios::class . ':showAddForm')
    ->setName('/servicios/registrar');
$app->post('/servicios/registrar', Controllers\Servicios::class . ':add')
    ->setName('/servicios/registrar');
$app->get('/servicios/buscar', Controllers\Servicios::class . ':search')
    ->setName('/servicios/buscar');
$app->get('/servicios/searchAjax', Controllers\Servicios::class . ':searchAjax')
    ->setName('/servicios/searchAjax');
$app->get('/servicios/editar/{id:[0-9]+}', Controllers\Servicios::class . ':showEditForm' )
    ->setName('/servicios/editar');
$app->post('/servicios/editar/{id:[0-9]+}', Controllers\Servicios::class . ':edit' )
    ->setName('/servicios/editar');
$app->get('/servicios/eliminar', Controllers\Servicios::class . ':erase')
    ->setName('/servicios/eliminar');

$app->get('/cuentas', Controllers\Cuentas::class . ':index' )
    ->setName('/cuentas');
$app->get('/cuentas/recuperar', Controllers\Cuentas::class . ':showRecoveryRequestForm')
    ->setName('/cuentas/recuperar');
$app->post('/cuentas/recuperar', Controllers\Cuentas::class . ':recoverRequest')
    ->setName('/cuentas/recuperar');
$app->get('/cuentas/recuperar/{token}', Controllers\Cuentas::class . ':showRecoveryForm')
    ->setName('/cuentas/recuperar');
$app->post('/cuentas/recuperar/{token}', Controllers\Cuentas::class . ':recover' )
    ->setName('/cuentas/recuperar');
$app->get('/cuentas/cuenta', Controllers\Cuentas::class . ':showAccountInfo')
    ->setName('/cuentas/cuenta');
$app->post('/cuentas/cuenta', Controllers\Cuentas::class . ':changePassword')
    ->setName('/cuentas/cuenta');
$app->get('/cuentas/listar[/{page:[0-9]+}]', Controllers\Cuentas::class . ':list')
    ->setName('/cuentas/listar');
$app->get('/cuentas/registrar', Controllers\Cuentas::class . ':showAddForm')
    ->setName('/cuentas/registrar');
$app->post('/cuentas/registrar', Controllers\Cuentas::class . ':add')
    ->setName('/cuentas/registrar');
$app->get('/cuentas/buscar[/{page:[0-9]+}]', Controllers\Cuentas::class . ':search')
    ->setName('/cuentas/buscar');
$app->get('/cuentas/editar/{id:[0-9]+}', Controllers\Cuentas::class . ':showEditForm' )
    ->setName('/cuentas/editar');
$app->post('/cuentas/editar/{id:[0-9]+}', Controllers\Cuentas::class . ':edit' )
    ->setName('/cuentas/editar');
$app->get('/cuentas/administrar', Controllers\Cuentas::class . ':manage')
    ->setName('/cuentas/administrar');
$app->post('/cuentas/administrar', Controllers\Cuentas::class . ':updatePermits')
    ->setName('/cuentas/administrar');
$app->get('/cuentas/rolesAjax/{rol:[0-9]+}', Controllers\Cuentas::class . ':rolAjax')
    ->setName('/cuentas/rolesAjax');

$app->get('/login', Controllers\Cuentas::class . ':showLoginForm')
    ->setName('/login');
$app->post('/login', Controllers\Cuentas::class . ':login')
    ->setName('/login');
$app->get('/logout', Controllers\Cuentas::class . ':logout')
    ->setName('/cuentas/cuenta');
