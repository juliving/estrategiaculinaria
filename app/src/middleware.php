<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);
use Juliving\EstrategiaCulinaria\Middleware as Middleware;

$app->add(new Middleware\Session($app->getContainer()));