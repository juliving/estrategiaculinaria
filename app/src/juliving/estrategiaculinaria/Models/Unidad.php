<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Unidad{

	private $db;

	public function __construct(){
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function getAll(){
        $sql=<<<SQL
SELECT simbolo, nombre
FROM unidad
SQL;
        $statement = $this->db->prepare($sql);
        //$statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_NUM);
    }


    public function getBySimbolo($simbolo){
        $sql=<<<SQL
SELECT nombre
FROM unidad 
WHERE simbolo =:simbolo
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':simbolo', $simbolo);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }
}