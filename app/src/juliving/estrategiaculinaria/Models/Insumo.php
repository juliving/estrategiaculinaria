<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Insumo
{

    private $db;

    public function __construct()
    {
            $this->db = DataBase::getInstance()->getConnection();
    }

    public function count()
    {
        $sql=<<<SQL
SELECT COUNT(1)
FROM insumo
SQL;
        $statement = $this->db->prepare($sql);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    public function getByPage($page)
    {
        $offset = ($page - 1) * 5;
        $sql=<<<SQL
SELECT *
FROM insumo
ORDER BY nombre
LIMIT 5 OFFSET :offset
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindValue(':offset', $offset, \PDO::PARAM_INT);
        
        $statement->execute();
        return [
            'result' => $statement->fetchAll(\PDO::FETCH_ASSOC),
            'count' => $this->count()
        ];
    }

    public function reporte()
    {
        $sql=<<<SQL
SELECT *
FROM insumo
SQL;
        $statement = $this->db->prepare($sql);
        $statement->execute();
        
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAll()
    {
        $sql=<<<SQL
SELECT *
FROM insumo
SQL;
        $statement = $this->db->prepare($sql);
        
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getById($id)
    {
            $sql=<<<SQL
			SELECT *
			FROM insumo
			WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function getByServicio($id)
    {
        $sql=<<<SQL
SELECT *
FROM insumo i, servicio_insumo si
WHERE si.servicio =:id
    AND i.id = si.insumo
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function eraseById($id)
    {
        $sql=<<<SQL
DELETE FROM insumo WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->rowCount();
    }

    public function create($insumo)
    {
        //De getParsedBody recibe todos los POST y lo recibe esta función en la variable insumo
        $proveedores = $insumo['proveedores'];
        unset($insumo['proveedores']);
        foreach ($proveedores as &$proveedor) {
            foreach ($proveedor as $key => $value) {
                $proveedor[':' . $key] = $value;
                unset($proveedor[$key]);
            }
        }
        //var_dump($proveedores);

        //Concatenar un : al nombre del campo (id)
        foreach ($insumo as $key => $value) {
            $insumo[':' . $key] = $value;
            unset($insumo[$key]);
        }


        try {
            // Permite revertir en caso de error
            $this->db->beginTransaction();

            // Insertar el registro en la tabla personas
            $sql=<<<SQL
	INSERT INTO insumo
	(nombre, descripcion, unidad)
	VALUES
	(:nombre, :descripcion, :unidad)
SQL;
                        //Prepara el contenido de sql como string para que se pueda ejecutar la consulta
            //$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
            $statement = $this->db->prepare($sql);

            $statement->execute($insumo);

            $id = $this->db->lastInsertId();

            $sql=<<<SQL
	INSERT INTO insumo_proveedor
	(insumo, proveedor, cantidad, presentacion, costo)
	VALUES
	(:insumo, :proveedor, :cantidad, :presentacion, :costo)
SQL;
            $statement = $this->db->prepare($sql);
            foreach ($proveedores as &$proveedor) {
                $proveedor[':insumo'] = $id;
                $statement->execute($proveedor);
            }

                $this->db->commit();
        } catch (\PDOException $e) {
            echo $e->getTraceAsString();
            $this->db->rollback();
            $id = false;
        }
            return $id;
    }



    /**
     * Actualiza la información del insumo en la base de datos
     *
     * @param array $insumo Información completa de la persona
     * @return bool La actualización se realizó correctamente
     */
    public function update(array $insumo){
        // Comprobar que el insumo a actualizar exista
        if (!isset($insumo['id']) || !$this->exists($insumo['id'])) {
            return false;
        }

        $id = $insumo['id'];

        // Extraer información correspondiente a tabla distinta a insumo
        $proveedores = $insumo['proveedores'];
        unset($insumo['proveedores']);
        foreach ($proveedores as &$proveedor) {
            foreach ($proveedor as $key => $value) {
                $proveedor[':' . $key] = $value;
                unset($proveedor[$key]);
            }
        }

        foreach ($insumo as $key => $value) {
            $insumo[':' . $key] = $value;
            unset($insumo[$key]);
        }
 
        try {
            // Permite revertir en caso de error
            $this->db->beginTransaction();

            // Insertar el registro en la tabla insumo
            $sql=<<<SQL
UPDATE insumo
SET nombre=:nombre,
    descripcion=:descripcion,
    unidad=:unidad
WHERE id=:id
SQL;
            $statement = $this->db->prepare($sql);
            $statement->execute($insumo);

        $sql=<<<SQL
DELETE FROM insumo_proveedor
WHERE insumo=:id
SQL;
            $statement = $this->db->prepare($sql);
            $statement->bindParam(":id", $insumo[':id']);
            $statement->execute();
            
            $sql=<<<SQL
INSERT INTO insumo_proveedor
(insumo, proveedor, cantidad, presentacion, costo)
VALUES
(:id, :proveedor, :cantidad, :presentacion, :costo)
SQL;
            // La sentencia $sql para la base de datos $this->db
            $statement = $this->db->prepare($sql);

            foreach ($proveedores as &$proveedor) {
                $proveedor[':id'] = $id;
                $statement->execute($proveedor);                
            }
            $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollback();
            var_dump($e->getMessage());
            var_dump($this->db->query("SHOW WARNINGS")->fetch());
            die();
            return false;
        }
        return true;    
    }


    public function search($q)
    {
        $q = '%'.$q.'%';
        $sql=<<<SQL
SELECT *
FROM insumo
WHERE nombre LIKE :q
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':q', $q);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function searchAutoComplete($q)
    {
            $q = '%'.$q.'%';
            $sql=<<<SQL
SELECT id as data, nombre as value
FROM insumo
WHERE nombre LIKE :q
SQL;
            $statement = $this->db->prepare($sql);
            $statement->bindParam(':q', $q);
            $statement->execute();
            return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Comprueba si el id especificado corresponde a un insumo en la base de datos
     *
     * @param int $id id del insumo a buscar
     * @return bool
     */
    public function exists(int $id)
    {
        $sql =<<<SQL
SELECT COUNT(1)
FROM insumo
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN) != 0;
    }
}
