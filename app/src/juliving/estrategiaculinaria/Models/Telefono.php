<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Telefono{

	private $db;

	public function __construct(){
			$this->db = DataBase::getInstance()->getConnection();
    }
    
    public function getByPerson($id){
        $sql=<<<SQL
SELECT numero, extension, tipo
FROM telefono
WHERE persona=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);

        $statement->execute();

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }
}
