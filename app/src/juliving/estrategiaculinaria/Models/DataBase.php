<?php

namespace Juliving\EstrategiaCulinaria\Models;


class DataBase{

    /**
     * Instancia del singleton
     *
     * @var DataBase
     */
    private static $instance;

    /**
     * @var \PDO Conexión a la base de datos
     */
    private $connection;

    /**
     * Carga la información de la conexión desde el archivo .env
     * Y genera la conexión usando PDO
     */
    private function __construct(){
        $host = \getenv('DB_HOST');
        $user = \getenv('DB_USER');
        $pass = \getenv('DB_PASSWORD');
        $database = \getenv('DB_NAME');
        $type = \getenv('DB_TYPE');

        $this->connection = new \PDO($type . ':host=' . $host . ";dbname=" . $database, $user, $pass, [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"]);
        $this->connection->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
    }

    /**
     * @return DataBase
     */
    public static function getInstance(){
        if(DataBase::$instance === null)
            DataBase::$instance = new DataBase();
        return DataBase::$instance;
    }

    /**
     * @return \PDO Conexión a la base de datos
     */
    public function getConnection(){
        return $this->connection;
    }


}
