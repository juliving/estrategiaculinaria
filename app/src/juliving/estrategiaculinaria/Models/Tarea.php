<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Tarea{

    private $db;
    
    public function __construct(){
            $this->db = DataBase::getInstance()->getConnection();
    }

    public function getByEvent($evento){
        $sql=<<<SQL
SELECT *
FROM tarea
WHERE evento=:evento
    AND padre IS NULL
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':evento', $evento);
        $statement->execute();
        $tasks = $statement->fetchAll(\PDO::FETCH_ASSOC);
        if(!$tasks)
            return false;
        $sql=<<<SQL
SELECT *
FROM tarea
WHERE padre=:padre
SQL;
        $statement = $this->db->prepare($sql);
        foreach($tasks as &$task){
            $statement->bindParam(':padre', $task['id']);
            $statement->execute();
            $task['subtareas'] = $statement->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $tasks;
    }

}