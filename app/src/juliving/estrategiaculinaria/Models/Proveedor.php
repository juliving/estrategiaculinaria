<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Proveedor{

    private $db;

    public function __construct()
    {
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function count()
    {
        $sql=<<<SQL
SELECT COUNT(1)
FROM proveedor
SQL;
        $statement = $this->db->prepare($sql);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    public function getByPage($page)
    {
        $offset = ($page - 1) * 5;
        $sql=<<<SQL
SELECT *
FROM proveedor
ORDER BY nombre
LIMIT 5 OFFSET :offset
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindValue(':offset', $offset, \PDO::PARAM_INT);
        
        $statement->execute();
        return [
            'result' => $statement->fetchAll(\PDO::FETCH_ASSOC),
            'count' => $this->count()
        ];
    }

    public function getAll()
    {
        $sql=<<<SQL
SELECT *
FROM proveedor
SQL;
        $statement = $this->db->prepare($sql);
        //$statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getById($id)
    {
        $sql=<<<SQL
SELECT *
FROM proveedor p
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function getByInsumo($id)
    {

        $sql=<<<SQL
SELECT *
FROM proveedor p, insumo_proveedor ip
WHERE ip.insumo = :id
    AND p.id = ip.proveedor
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getMinimumByInsumo($id)
    {

        $sql=<<<SQL
SELECT p.id, p.nombre, ip.presentacion, ip.cantidad, ip.costo
FROM proveedor p, insumo_proveedor ip
WHERE ip.insumo = :id
    AND p.id = ip.proveedor
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getByServicioSelect($id){
        $sql=<<<SQL
SELECT id, CONCAT(nombre, CONCAT(' - $', costo)) AS nombre
FROM proveedor p, servicio_proveedor sp
WHERE sp.servicio = :id
    AND sp.proveedor = p.id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getByServicio($id)
    {
        $sql=<<<SQL
SELECT *
FROM proveedor p, servicio_proveedor sp
WHERE sp.servicio = :id
    AND sp.proveedor = p.id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function search($q){
        $q = '%'.$q.'%';
        $sql=<<<SQL
SELECT *
FROM proveedor
WHERE nombre LIKE :q
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':q', $q);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function searchAutoComplete($q){
        $q = '%'.$q.'%';
        $sql=<<<SQL
SELECT id as data, nombre as value
FROM proveedor
WHERE nombre LIKE :q
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':q', $q);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }
}
