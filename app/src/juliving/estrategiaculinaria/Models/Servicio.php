<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Servicio
{

    private $db;

    public function __construct()
    {
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function count()
    {
        $sql=<<<SQL
SELECT COUNT(1)
FROM servicio
SQL;
        $statement = $this->db->prepare($sql);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    public function getByPage($page)
    {
        $offset = ($page - 1) * 5;
        $sql=<<<SQL
SELECT *
FROM servicio
ORDER BY nombre
LIMIT 5 OFFSET :offset
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindValue(':offset', $offset, \PDO::PARAM_INT);
        
        $statement->execute();
        return [
            'result' => $statement->fetchAll(\PDO::FETCH_ASSOC),
            'count' => $this->count()
        ];
    }

    public function getCostByProvider($id, $proveedor){
        $sql=<<<SQL
SELECT costo
FROM servicio_proveedor
WHERE servicio=:id
AND proveedor=:proveedor
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->bindParam(':proveedor', $proveedor);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    public function eraseById($id)
    {
        $sql=<<<SQL
DELETE FROM servicio WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        
        return $statement->rowCount(); 
    }

    public function getAll()
    {
        $sql=<<<SQL
SELECT *
FROM servicio
SQL;
        $statement = $this->db->prepare($sql);
        //$statement->bindParam(':id', $id);

        $statement->execute();

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getById($id)
    {

        $sql=<<<SQL
SELECT *
FROM servicio
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function create($servicio)
    {

      //Primero debo obtener los diferentes arreglos de información y hacer unset
      //Para los arreglos que a su vez contienen arreglos, recorrerlos y concaternarles un :
        $insumos = $servicio['insumos'];
        unset($servicio['insumos']);
        foreach ($insumos as &$insumo) {
            foreach ($insumo as $key => $value) {
                $insumo[':' . $key] = $value; //Crear una nueva localidad para que sea :key
                unset($insumo[$key]); //Destruir la localdiad que no tenga los :
            }
        }

        $proveedores = $servicio['proveedores'];
        unset($servicio['proveedores']);
        foreach ($proveedores as &$proveedor) {
            foreach ($proveedor as $key => $value) {
                $proveedor[':' . $key] = $value;
                unset($proveedor[$key]);
            }
        }

        $actividades = $servicio['actividades'];
        unset($servicio['actividades']);

      //Concatenar un : al nombre del campo (id)
        foreach ($servicio as $key => $value) {
            $servicio[':' . $key] = $value;
            unset($servicio[$key]);
        }

        try {
            // Permite revertir en caso de error
            $this->db->beginTransaction();

            // Insertar en tabla servicio
            $sql=<<<SQL
INSERT INTO servicio
(nombre, descripcion, precio)
VALUES
(:nombre, :descripcion, :precio)
SQL;
            //Prepara el contenido de sql como string para que se pueda ejecutar la consulta
            $statement = $this->db->prepare($sql);
            $statement->execute($servicio); //Ejecuta la instrucción de SQL

            //Último ID del servicio
            $id = $this->db->lastInsertId();
            
            // Insertar en tabla servicio_actividad
            $sql=<<<SQL
INSERT INTO actividad
(servicio, nombre, descripcion, prioridad)
VALUES
(:servicio, :nombre, :descripcion, :prioridad)
SQL;

            $sqls=<<<SQL
INSERT INTO actividad
(servicio, nombre, padre, descripcion, prioridad)
VALUES
(:servicio, :nombre, :padre, :descripcion, :prioridad)
SQL;

            foreach ($actividades as &$actividad) {
                //Prepara el contenido de sql como string para que se pueda ejecutar la consulta
                $statement = $this->db->prepare($sql);
                $subactividades = $actividad['subactividad'];
                unset($actividad['subactividad']);
                foreach ($actividad as $key => $value) {
                    $actividad[':' . $key] = $value;
                    unset($actividad[$key]);
                }
                foreach ($subactividades as &$subactividad) {
                    foreach ($subactividad as $key => $value) {
                        $subactividad[':' . $key] = $value;
                        unset($subactividad[$key]);
                    }
                }
                
                $actividad[':servicio'] = $id;

                $statement->execute($actividad);

                $ida = $this->db->lastInsertId();
                $statement = $this->db->prepare($sqls);

                foreach ($subactividades as &$subactividad) {
                    $subactividad[':servicio'] = $id;
                    $subactividad[':padre'] = $ida;
                    
                    $statement->execute($subactividad);
                    //var_dump($subactividad);
                    
                }
            }
            //die();

            // Insertar en tabla servicio_insumo
            $sql=<<<SQL
INSERT INTO servicio_insumo
(servicio, insumo, cantidad)
VALUES
(:servicio, :insumo, :cantidad)
SQL;
            
            //Prepara el contenido de sql como string para que se pueda ejecutar la consulta
            $statement = $this->db->prepare($sql);
            foreach ($insumos as &$insumo) {
                $insumo[':servicio'] = $id;
                $statement->execute($insumo);
                //var_dump($this->db->query("SHOW WARNINGS")->fetch()); Para mostrar warnings 
            }

            // Insertar en tabla servicio_proveedor
            $sql=<<<SQL
INSERT INTO servicio_proveedor
(servicio, proveedor, costo)
VALUES
(:servicio, :proveedor, :costo)
SQL;
            //Prepara el contenido de sql como string para que se pueda ejecutar la consulta
            $statement = $this->db->prepare($sql);
            foreach ($proveedores as &$proveedor) {
                $proveedor[':servicio'] = $id;
                $statement->execute($proveedor);
            }

            $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollback();
            var_dump($e->getMessage());
            var_dump($this->db->query("SHOW WARNINGS")->fetch());
            die();
            $id = false;
        }
        return $id;
    }



    /**
     * Actualiza la información del insumo en la base de datos
     *
     * @param array $servicio Información completa de la persona
     * @return bool La actualización se realizó correctamente
     */
    public function update(array $servicio)
    {
        // Comprobar que el servicio a actualizar exista
        if (!isset($servicio['id']) || !$this->exists($servicio['id'])) {
            return false;
        }

        $id = $servicio['id'];

        // Extraer información correspondiente a tabla distinta a servicio
        $insumos = $servicio['insumos'];
        unset($servicio['insumos']);
        foreach ($insumos as &$insumo) {
            foreach ($insumo as $key => $value) {
                $insumo[':' . $key] = $value;
                unset($insumo[$key]);
            }
        }

        $proveedores = $servicio['proveedores'];
        unset($servicio['proveedores']);
        foreach ($proveedores as &$proveedor) {
            foreach ($proveedor as $key => $value) {
                $proveedor[':' . $key] = $value;
                unset($proveedor[$key]);
            }
        }

        $actividades = $servicio['actividades'];
        unset($servicio['actividades']);
        /*foreach ($actividades as &$actividad) {
            foreach ($actividad as $key => $value) {
                $actividad[':' . $key] = $value;
                unset($actividad[$key]);
            }
            foreach ($actividad[':subactividades'] as &$subactividad) {
                    foreach ($subactividad as $key => $value) {
                        $subactividad[':' .$key] = $value;
                        unset($subactividad[$key]);
                    }
            }
        }*/

        foreach ($actividades as &$actividad) {
                $subactividades = $actividad['subactividad'];
                unset($actividad['subactividad']);
                foreach ($actividad as $key => $value) {
                    $actividad[':' . $key] = $value;
                    unset($actividad[$key]);
                }
                foreach ($subactividades as &$subactividad) {
                    foreach ($subactividad as $key => $value) {
                        $subactividad[':' . $key] = $value;
                        unset($subactividad[$key]);
                    }
                }
        }



        foreach ($servicio as $key => $value) {
            $servicio[':' . $key] = $value;
            unset($servicio[$key]);
        }
 
        try {
            // Permite revertir en caso de error
            $this->db->beginTransaction();

            // Insertar el registro en la tabla insumo
            $sql=<<<SQL
UPDATE servicio
SET nombre=:nombre,
    descripcion=:descripcion,
    precio=:precio
WHERE id=:id
SQL;
            $statement = $this->db->prepare($sql);
            $statement->execute($servicio);


        //Insertar en servicio_insumo
        $sql=<<<SQL
DELETE FROM servicio_insumo
WHERE servicio=:id
SQL;
            $statement = $this->db->prepare($sql);
            $statement->bindParam(":id", $servicio[':id']);
            $statement->execute();
            
            $sql=<<<SQL
INSERT INTO servicio_insumo
(servicio, insumo, cantidad)
VALUES
(:servicio, :insumo, :cantidad)
SQL;
            // La sentencia $sql para la base de datos $this->db
            $statement = $this->db->prepare($sql);

            foreach ($insumos as &$insumo) {
                $insumo[':servicio'] = $id;
                $statement->execute($insumo);                
            }



        //Insertar en servicio_proveedor
        $sql=<<<SQL
DELETE FROM servicio_proveedor
WHERE servicio=:id
SQL;
            $statement = $this->db->prepare($sql);
            $statement->bindParam(":id", $servicio[':id']);
            $statement->execute();
            
            $sql=<<<SQL
INSERT INTO servicio_proveedor
(servicio, proveedor, costo)
VALUES
(:servicio, :proveedor, :costo)
SQL;
            // La sentencia $sql para la base de datos $this->db
            $statement = $this->db->prepare($sql);

            foreach ($proveedores as &$proveedor) {
                $proveedor[':servicio'] = $id;
                $statement->execute($proveedor);                
            }

        //Insertar en actividad
        $sql=<<<SQL
UPDATE actividad
SET servicio= :servicio
    nombre=:nombre
    descripcion=:descripcion
    prioridad=:prioridad
SQL;


        $sqls=<<<SQL

UPDATE actividad
SET servicio= :servicio
    nombre= :nombre
    padre= :padre
    descripcion= :descripcion
    prioridad= :prioridad
WHERE id=:id
SQL;
            //Prepara el contenido de sql como string para que se pueda ejecutar la consulta
            $statement = $this->db->prepare($sql);
            $statement->bindParam(":id", $servicio[':id']);

            $actividades = $servicio['actividades'];
            unset($servicio['actividades']);
            foreach ($actividades as &$actividad) {
                $subactividades = $actividad['subactividad'];
                unset($actividad['subactividad']);
                
                $actividad[':servicio'] = $id;

                $statement->execute($actividad);

                $ida = $this->db->lastInsertId();
                $statement = $this->db->prepare($sqls);
                foreach ($subactividades as &$subactividad) {
                    $subactividad[':servicio'] = $id;
                    $subactividad[':padre'] = $ida;
                    $statement->execute($subactividad);
                }
            }
            $this->db->commit();
        }catch (\PDOException $e) {
            $this->db->rollback();
            var_dump($e->getMessage());
            var_dump($this->db->query("SHOW WARNINGS")->fetch());
            die();
            return false;
        }
        return true;

    }

    public function search($q)
    {
        $q = '%'.$q.'%';
        $sql=<<<SQL
SELECT *
FROM servicio
WHERE nombre LIKE :q
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':q', $q);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function searchAutoComplete($q)
    {
        $q = '%'.$q.'%';
        $sql=<<<SQL
SELECT id AS `data`, nombre AS `value`, precio 
FROM servicio
WHERE nombre LIKE :q
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':q', $q);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Comprueba si el id especificado corresponde a un servicio en la base de datos
     *
     * @param int $id id del servicio a buscar
     * @return bool
     */
    public function exists(int $id)
    {
        $sql =<<<SQL
SELECT COUNT(1)
FROM servicio
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN) != 0;
    }
}
