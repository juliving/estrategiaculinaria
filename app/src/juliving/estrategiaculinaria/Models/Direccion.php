<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Direccion{

    private $db;

    public function __construct(){
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function getById($id){
        $sql=<<<SQL
SELECT calle, exterior, interior, colonia, cp
FROM direccion
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

}
