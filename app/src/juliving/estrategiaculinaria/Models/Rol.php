<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Rol{

    private $db;

    public function __construct(){
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function getAll()
    {
        $sql=<<<SQL
SELECT *
FROM rol
SQL;
        $statement = $this->db->prepare($sql);
        
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAllCR($id)
    {
        $sql=<<<SQL
SELECT rol
FROM cuenta_rol
WHERE cuenta=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }
}