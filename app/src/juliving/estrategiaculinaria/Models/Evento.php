<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Evento{

    private $db;

    public function __construct(){
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function create($evento){

        $lugar = $evento['lugar'];
        unset($evento['lugar']);

        $servicios = $evento['servicios'];
        unset($evento['servicios']);

        $empleados = $evento['empleados'];
        unset($evento['empleados']);

        $contactos = $evento['contactos'];
        unset($evento['contactos']);

        foreach($evento as $key => $value){
            $evento[':' . $key] = $value;            
            unset($evento[$key]);
        }

        foreach($lugar as $key => $value){
            $lugar[':' . $key] = $value;
            unset($lugar[$key]);
        }

        foreach($servicios as &$servicio){
            foreach($servicio as $key => $value){
                $servicio[':' . $key] = $value;
                unset($servicio[$key]);
            }
        }

        foreach($empleados as &$empleado){
            foreach($empleado as $key => $value){
                $empleado[':' . $key] = $value;
                unset($empleado[$key]);
            }
        }

        foreach($contactos as &$contacto){
            foreach($contacto as $key => $value){
                $contacto[':' . $key] = $value;
                unset($contacto[$key]);
            }
        }
        
        try{
            $this->db->beginTransaction();
            
            $sql =<<<SQL
INSERT INTO direccion (exterior, interior, calle, colonia, cp, latitud, longitud)
VALUES (:exterior, :interior, :calle, :colonia, :cp, :latitud, :longitud)
SQL;
            $statement = $this->db->prepare($sql);
            $statement->execute($lugar);
            $evento[':lugar'] = $this->db->lastInsertId();
            
            $sql =<<<SQL
INSERT INTO evento (nombre, cliente, lugar, descripcion, fecha, personas, estado, costos_fijos)
VALUES (:nombre, :cliente, :lugar, :descripcion, :fecha, :personas, :estado, :costos_fijos)
SQL;
            $statement = $this->db->prepare($sql);
            $statement->execute($evento);
            $id = $this->db->lastInsertId();
            $sql =<<<SQL
INSERT INTO evento_servicio (evento, servicio, proveedor, cantidad, costo, precio)
VALUES (:evento, :servicio, :proveedor, :cantidad, :costo, :precio)
SQL;
            $statement = $this->db->prepare($sql);
            $modeloServicio = new Servicio();
            foreach($servicios as &$servicio){
                $servicio[':costo'] = $modeloServicio->getCostByProvider($servicio[':servicio'], $servicio[':proveedor']);
                $servicio[':precio'] = $servicio[':precio'] / $servicio[':cantidad'];
                $servicio[':evento'] = $id;
                $statement->execute($servicio);
            }
            $sql =<<<SQL
INSERT INTO evento_empleado (evento, empleado, puesto, turnos, salario)
VALUES (:evento, :empleado, :puesto, :turnos, :salario)
SQL;
            $statement = $this->db->prepare($sql);
            foreach($empleados as &$empleado){
                $empleado[':evento'] = $id;
                $statement->execute($empleado);
            }
            $sql =<<<SQL
INSERT INTO contacto (evento, nombre, descripcion, telefono, correo)
VALUES (:evento, :nombre, :descripcion, :telefono, :correo)
SQL;
            $statement = $this->db->prepare($sql);

            foreach($contactos as &$contacto){
                $contacto[':evento'] = $id;
                $statement->execute($contacto);
            }
            
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            echo $ex->getMessage();
            die();
            $id = false;
        }
        return $id;       
    }

    public function eraseById($id)
    {
try {
        $this->db->beginTransaction();

        $sql=<<<SQL
SELECT domicilio
FROM persona
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();

        $direccion = $statement->fetch(\PDO::FETCH_COLUMN);

        $sql=<<<SQL
DELETE FROM evento
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();

        $sql=<<<SQL
DELETE FROM direccion WHERE id=:dir
SQL;
                $statement = $this->db->prepare($sql);
                $statement->bindParam(':dir', $direccion);
                $statement->execute();

        $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollback();
            var_dump($e->getMessage());
            var_dump($this->db->query("SHOW WARNINGS")->fetch());
            die();
            return false;
        }
        return true;
        }

    public function count()
    {
        $sql=<<<SQL
SELECT COUNT(1)
FROM evento
SQL;
        $statement = $this->db->prepare($sql);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    public function getByPage($page)
    {
        $offset = ($page - 1) * 5;
        $sql=<<<SQL
SELECT *
FROM evento
ORDER BY fecha DESC
LIMIT 5 OFFSET :offset
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindValue(':offset', $offset, \PDO::PARAM_INT);
        
        $statement->execute();
        return [
            'result' => $statement->fetchAll(\PDO::FETCH_ASSOC),
            'count' => $this->count()
        ];
    }

    public function getById($id){

        $sql=<<<SQL
SELECT *
FROM evento
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function getByClient($id, $limit=5){

        $sql=<<<SQL
SELECT e.nombre, e.personas, (SUM(es.precio) + e.costos_fijos) as total
FROM evento e, evento_servicio es
WHERE e.id=es.evento AND e.cliente=:id
LIMIT :cantidad
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->bindValue(':cantidad', $limit, \PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);

    }


    public function getByEmpleado($id, $limit=5){

        $sql=<<<SQL
SELECT E.nombre, SUM(Ep.salario) as salario
FROM evento_empleado Ep, evento E
WHERE Ep.evento = E.id AND  Ep.empleado=:id 
GROUP BY E.nombre
LIMIT :cantidad
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->bindValue(':cantidad', $limit, \PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }


    public function getByProveedor($id, $limit=5){

        $sql=<<<SQL
SELECT DISTINCT p.nombre as proveedor, e.nombre as nombreEvento
FROM proveedor p, evento e, insumo_proveedor ip, servicio_proveedor sp, evento_servicio es, servicio_insumo si
WHERE ((e.id = es.evento AND es.servicio = sp.servicio AND sp.proveedor = p.id AND p.id = :id) OR 
        (e.id = es.evento AND es.servicio = si.servicio AND si.insumo = ip.insumo AND ip.insumo = p.id AND p.id = :id))
    AND e.estado = 'confirmado'
LIMIT :cantidad
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->bindValue(':cantidad', $limit, \PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }


    public function getAll(){
      $sql=<<<SQL
SELECT *
FROM evento
SQL;
      $statement = $this->db->prepare($sql);
      $statement->execute();
      return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getByDateRange($start, $end){
        $sql=<<<SQL
SELECT *
FROM evento
WHERE fecha BETWEEN :start AND :end
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindValue(':start', $start, \PDO::PARAM_STR);
        $statement->bindValue(':end', $end, \PDO::PARAM_STR);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function search($q){
        $q = '%'.$q.'%';
        $sql=<<<SQL
SELECT *
FROM evento
WHERE nombre LIKE :q
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':q', $q);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function searchAutoComplete($q){
        $q = '%'.$q.'%';
        $sql=<<<SQL
SELECT id as data, nombre as value
FROM evento
WHERE nombre LIKE :q
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':q', $q);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function updateDate(array $tarea){
            if($tarea['completada'] == 'true'){
                $sql=<<<SQL
UPDATE tarea
SET completada = now()
WHERE id=:id
SQL;
            }
            else{
                $sql=<<<SQL
UPDATE tarea
SET completada = NULL
WHERE id=:id
SQL;
            }

        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $tarea['id']);
        $statement->execute();
        
    }

}
