<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Actividad{

    private $db;

    public function __construct(){
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function getByServicio($id){
		$sql=<<<SQL
SELECT *
FROM actividad a
WHERE a.padre IS NULL
	AND a.servicio = :id
SQL;
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':id', $id);
		$statement->execute();
		$padres = $statement->fetchAll(\PDO::FETCH_ASSOC);
		
		if ($padres) {
		$sql=<<<SQL
SELECT *
FROM actividad a
WHERE a.padre = :padre
SQL;

		$statement = $this->db->prepare($sql);

		foreach ($padres as &$padre) {
			$statement->bindParam(':padre', $padre['id']);
			$statement->execute();
			$padre['subactividades'] = $statement->fetchAll(\PDO::FETCH_ASSOC);;
		}

			return $padres;
		}
	}
}
