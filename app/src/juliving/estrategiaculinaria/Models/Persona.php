<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Persona
{

    private $db;

    public function __construct()
    {
        $this->db = DataBase::getInstance()->getConnection();
    }

    /**
     * Regresa un array conteniendo la información de la persona o false si no existe
     *
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {

        $sql=<<<SQL
SELECT *
FROM persona
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        $persona = $statement->fetch(\PDO::FETCH_ASSOC);

        if (!$persona) {
            return false;
        }

        $sql=<<<SQL
SELECT tipo_persona
FROM persona_tipo_persona
WHERE persona=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $persona['id']);
        $statement->execute();
        $persona['tipo'] = $statement->fetchAll(\PDO::FETCH_COLUMN);

        $sql=<<<SQL
SELECT calle, exterior, interior, colonia, cp, latitud, longitud
FROM direccion
WHERE id=:domicilio
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':domicilio', $persona['domicilio']);
        $statement->execute();
        $persona['domicilio'] = $statement->fetch(\PDO::FETCH_ASSOC);

        return $persona;
    }

    /**
     * Comprueba si el id especificado corresponde a una persona en la base de datos
     *
     * @param int $id id de la persona a buscar
     * @return bool
     */
    public function exists(int $id)
    {
        $sql =<<<SQL
SELECT COUNT(1)
FROM persona
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN) != 0;
    }

    /**
     * Regresa el id de el domicilio de la persona especificada
     *
     * @param int $id
     * @return mixed id de la direccion o false si la persona no existe
     */
    public function getAddressId(int $id)
    {
        $sql =<<<SQL
SELECT domicilio
FROM persona
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    /**
     * Actualiza la información de la persona en la base de datos
     *
     * @param array $persona Información completa de la persona
     * @return bool La actualización se realizó correctamente
     */
    public function update(array $persona)
    {
        // Comprobar que la persona a actualizar exista
        if (!isset($persona['id']) || !$this->exists($persona['id'])) {
            return false;
        }

        $id = $persona['id'];
        
        // Extraer información correspondiente a tablas distintas a persona
        $tipos = $persona['tipo'];
        unset($persona['tipo']);

        $direccion = $persona['direccion'];
        $direccion['id'] = $this->getAddressId($id);
        unset($persona['direccion']);
        foreach ($direccion as $key => $value) {
            $direccion[':' . $key] = $value;
            unset($direccion[$key]);
        }

        $telefonos = $persona['telefonos'];
        unset($persona['telefonos']);
        foreach ($telefonos as &$telefono) {
            foreach ($telefono as $key => $value) {
                $telefono[':' . $key] = $value;
                unset($telefono[$key]);
            }
        }

        foreach ($persona as $key => $value) {
            $persona[':' . $key] = $value;
            unset($persona[$key]);
        }
        try {
            // Permite revertir en caso de error
            $this->db->beginTransaction();

            // Actualizar la direccion

            $sql=<<<SQL
UPDATE direccion
SET exterior=:exterior,
    interior=:interior,
    calle=:calle,
    colonia=:colonia,
    latitud=:latitud,
    longitud=:longitud,
    cp=:cp
WHERE id=:id
SQL;
            $statement = $this->db->prepare($sql);
            $statement->execute($direccion);
            // Insertar el registro en la tabla personas
            $sql=<<<SQL
UPDATE persona
SET rfc=:rfc,
    nombre=:nombre,
    correo=:correo
WHERE id=:id
SQL;
            $statement = $this->db->prepare($sql);
            $statement->execute($persona);
            
            $sql=<<<SQL
DELETE FROM telefono
WHERE persona=:id
SQL;
            $statement = $this->db->prepare($sql);
            $statement->bindParam(":id", $persona[':id']);
            $statement->execute();
            
            $sql=<<<SQL
INSERT INTO telefono
(persona, tipo, numero, extension)
VALUES
(:persona, :tipo, :numero, :extension)
SQL;
            // La sentencia $sql para la base de datos $this->db
            $statement = $this->db->prepare($sql);

            foreach ($telefonos as &$telefono) {
                $telefono[':persona'] = $id;
                $statement->execute($telefono);                
            }
            $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollback();
            var_dump($this->db->query("SHOW WARNINGS")->fetch());
            die();
            return false;
        }
        return true;
    }

    public function create($persona)
    {

        // Extraer información correspondiente a tablas distintas a persona
        $tipos = $persona['tipo'];
        unset($persona['tipo']);

        $direccion = $persona['direccion'];
        unset($persona['direccion']);
        foreach ($direccion as $key => $value) {
            $direccion[':' . $key] = $value;
            unset($direccion[$key]);
        }

        $telefonos = $persona['telefonos'];
        unset($persona['telefonos']);
        foreach ($telefonos as &$telefono) {
            foreach ($telefono as $key => $value) {
                $telefono[':' . $key] = $value;
                unset($telefono[$key]);
            }
        }

        foreach ($persona as $key => $value) {
            $persona[':' . $key] = $value;
            unset($persona[$key]);
        }
        try {
            // Permite revertir en caso de error
            $this->db->beginTransaction();

            // Insertar la direccion a la base de datos
            $sql=<<<SQL
INSERT INTO direccion
(exterior, interior, calle, colonia, cp, latitud, longitud)
VALUES
(:exterior, :interior, :calle, :colonia, :cp, :latitud, :longitud)
SQL;
            $statement = $this->db->prepare($sql);
            $statement->execute($direccion);
            // id de la nueva direccion
            $persona[':domicilio'] = $this->db->lastInsertId();

            // Insertar el registro en la tabla personas
            $sql=<<<SQL
INSERT INTO persona
(rfc, nombre, correo, domicilio)
VALUES
(:rfc, :nombre, :correo, :domicilio)
SQL;
            $statement = $this->db->prepare($sql);
            $statement->execute($persona);
            // id de la nueva persona
            $id = $this->db->lastInsertId();

            // Insertar registro de tipo de persona
            $sql=<<<SQL
INSERT INTO persona_tipo_persona
(persona, tipo_persona)
VALUES
(:persona, :tipo_persona)
SQL;
            // La sentencia $sql para la base de datos $this->db
            $statement = $this->db->prepare($sql);
            foreach ($tipos as $tipo) {
                $statement->bindParam(':persona', $id);
                $statement->bindParam(':tipo_persona', $tipo);
                $statement->execute();
            }
            
            // Insertar registros de teléfonos
            $sql=<<<SQL
INSERT INTO telefono
(persona, tipo, numero, extension)
VALUES
(:persona, :tipo, :numero, :extension)
SQL;
            // La sentencia $sql para la base de datos $this->db
            $statement = $this->db->prepare($sql);

            foreach ($telefonos as &$telefono) {
                $telefono[':persona'] = $id;
                $statement->execute($telefono);
            }
            $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollback();
            $id = false;
        }
        return $id;
    }

    public function eraseById($id)
    {
try {
        $this->db->beginTransaction();

        $sql=<<<SQL
SELECT domicilio
FROM persona
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();

        $direccion = $statement->fetch(\PDO::FETCH_COLUMN);

        $sql=<<<SQL
DELETE FROM persona
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();

        $sql=<<<SQL
DELETE FROM direccion WHERE id=:dir
SQL;
                $statement = $this->db->prepare($sql);
                $statement->bindParam(':dir', $direccion);
                $statement->execute();

        $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollback();
            var_dump($e->getMessage());
            var_dump($this->db->query("SHOW WARNINGS")->fetch());
            die();
            return false;
        }
        return true;
        }

    public function getAll($tipo)
    {
    }

    public function getEMailById($id)
    {
        $sql=<<<SQL
SELECT correo
FROM persona
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    public function getNameById($id)
    {
        $sql=<<<SQL
SELECT nombre
FROM persona
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }
}
