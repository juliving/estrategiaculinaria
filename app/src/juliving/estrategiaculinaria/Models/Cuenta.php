<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Cuenta{

    public const DEFAULT_ROLE = 1;
    private $db;

    public function __construct(){
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function getAllRol()
    {
        $sql=<<<SQL
SELECT *
FROM rol
SQL;
        $statement = $this->db->prepare($sql);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function searchActual($id)
    {
        $sql=<<<SQL
SELECT p.nombre
FROM permiso p, rol_permiso rp, rol r
WHERE rp.permiso = p.nombre 
    AND  rp.rol = r.id 
    AND r.id =:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function getAllPermiso()
    {
        $sql=<<<SQL
SELECT *
FROM permiso
WHERE asignable = TRUE
SQL;
        $statement = $this->db->prepare($sql);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function count()
    {
        $sql=<<<SQL
SELECT COUNT(1)
FROM cuenta
SQL;
        $statement = $this->db->prepare($sql);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }
    
    public function updatePermits(array $permisos){

        // foreach ($permiso as $key => $value) {
        //     $permiso[':' . $key] = $value;
        //     unset($permiso[$key]);
        // }

        $rol = $permisos['rol'];
        $permisos=$permisos['permisos'];
        try {
            // Permite revertir en caso de error
            $this->db->beginTransaction();

        $sql=<<<SQL
DELETE FROM rol_permiso
WHERE rol=:rol
SQL;
            $statement = $this->db->prepare($sql);
            $statement->bindParam(':rol', $rol);
            $statement->execute();
            
        $sql=<<<SQL
INSERT INTO rol_permiso
(rol, permiso)
VALUES
(:rol, :permiso)
SQL;
            // La sentencia $sql para la base de datos $this->db
            $statement = $this->db->prepare($sql);

            foreach ($permisos as &$permit) {
                $statement->bindParam(':rol', $rol);
                $statement->bindParam(':permiso', $permit);
                $statement->execute();                
            }
            $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollback();
            var_dump($e->getMessage());
            var_dump($this->db->query("SHOW WARNINGS")->fetch());
            die();
            return false;
        }
        return true;    
    }

    public function getByPage($page)
        {
            $offset = ($page - 1) * 5;
    
        $sql=<<<SQL
SELECT c.usuario as 'usuario', e.nombre as 'nombrep', c.id
FROM cuenta c, empleado e
WHERE c.id = e.id 
ORDER BY usuario
LIMIT 5 OFFSET :offset
SQL;
            $statement = $this->db->prepare($sql);
            $statement->bindValue(':offset', $offset, \PDO::PARAM_INT);
            
            $statement->execute();
            $usuarios = $statement->fetchAll(\PDO::FETCH_ASSOC);
            
            if ($usuarios) {
        $sql=<<<SQL
SELECT *
FROM rol r, cuenta_rol cr
WHERE r.id = cr.rol AND cr.cuenta=:id
SQL;

                $statement = $this->db->prepare($sql);
        
                foreach ($usuarios as &$usuario) {
                    $statement->bindParam(':id', $usuario['id']);
                    $statement->execute();
                    $usuario['rol'] = $statement->fetchAll(\PDO::FETCH_ASSOC);
                }
    
            return [
                'result' => $usuarios,
                'count' => $this->count()
            ];
            }
        }

    public function getByPageE($page)
    {
        $offset = ($page - 1) * 5;
        $sql=<<<SQL
SELECT *
FROM cuenta
ORDER BY usuario
LIMIT 5 OFFSET :offset
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindValue(':offset', $offset, \PDO::PARAM_INT);
        
        $statement->execute();
        return [
            'count' => $this->count(),
        ];
    }

    public function getById($id)
    {
        $sql=<<<SQL
SELECT usuario, id
FROM cuenta
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function getByUser($user){
        $sql=<<<SQL
SELECT id, usuario, contrasenia, sal
FROM cuenta
WHERE usuario=:usuario
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':usuario', $user);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function getIdByUser($user){
        $sql=<<<SQL
SELECT id
FROM cuenta
WHERE usuario=:usuario
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':usuario', $user);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    public function create($cuenta, $password, $salt)
    {
        //Concatenar un : al nombre del campo (id)
        foreach ($cuenta as $key => $value) {
            $cuenta[':' . $key] = $value;
            unset($cuenta[$key]);
        }

        unset($cuenta[':contrasenia']);
        $cuenta[':contrasenia'] = $password;
        $cuenta[':sal'] = $salt;
        $roles = array(':rol'=>$cuenta[':rol'],':cuenta'=>$cuenta[':id']);
        unset($cuenta[':rol']);

        try {
            // Permite revertir en caso de error
            $this->db->beginTransaction();
        
        //Al correr la sentencia, si los campos pueden ser nulos, no se pasan en el VALUES
        $sql=<<<SQL
    INSERT INTO cuenta
    (id, usuario, contrasenia, sal)
    VALUES
    (:id, :usuario, :contrasenia, :sal)
SQL;
                    $statement = $this->db->prepare($sql);
                    //Excecute no es compatible con bindParams, mejor insertarlos arriba en el arreglo
                    $statement->execute($cuenta);

                    $id = $cuenta[':id'];
            
        $sql=<<<SQL
    INSERT INTO cuenta_rol
    (cuenta, rol)
    VALUES
    (:cuenta, :rol)
SQL;

                $statement = $this->db->prepare($sql);

                $statement->execute($roles);

                $this->db->commit();

        } catch (\PDOException $e) {
            echo $e->getMessage();
            echo $e->getTraceAsString();
            $this->db->rollback();
            $id = false;
        }
            return $id;
    }

    /**
     * Actualiza la información del cuenta en la base de datos
     *
     * @param array $cuenta Información completa de la persona
     * @return bool La actualización se realizó correctamente
     */
    public function update($cuenta, $password, $salt){
        // Comprobar que el cuenta a actualizar exista
        if (!isset($cuenta['id']) || !$this->exists($cuenta['id'])) {
            return false;
        }

        foreach ($cuenta as $key => $value) {
            $cuenta[':' . $key] = $value;
            unset($cuenta[$key]);
        }

        $id = $cuenta[':id'];
        unset($cuenta[':id']);
        unset($cuenta[':contrasenia']);

        $roles = $cuenta[':rol'];
        unset($cuenta[':rol']);
        
        try {
            // Permite revertir en caso de error
            $this->db->beginTransaction();


            // Insertar el registro en la tabla cuenta
            $sql=<<<SQL
UPDATE cuenta
SET usuario=:usuario,
    contrasenia=:contrasenia,
    sal=:sal,
    recuperar=NULL,
    recuperar_hasta=NULL
WHERE id=:id
SQL;
            $statement = $this->db->prepare($sql);
            $statement->bindParam(':contrasenia', $password);
            $statement->bindParam(':sal', $salt);
            $statement->bindParam(':id', $id);
            $statement->bindParam(':usuario', $cuenta[':usuario']);
            $statement->execute();

$sql=<<<SQL
DELETE FROM cuenta_rol
WHERE cuenta=:id
SQL;
            $statement = $this->db->prepare($sql);
            $statement->bindParam(":id", $id);
            $statement->execute();
            
            $sql=<<<SQL
INSERT INTO cuenta_rol
(cuenta, rol)
VALUES
(:cuenta, :rol)
SQL;
            // La sentencia $sql para la base de datos $this->db
            $statement = $this->db->prepare($sql);

            foreach ($roles as $rol) {
                //No se pueden mezclar bindParam y excecute con variable, o son todos bind o todo está en la variable del execute
                $statement->bindParam(":cuenta", $id);
                $statement->bindParam(":rol", $rol);
                $statement->execute();                
            }

            $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollback();
            var_dump($e->getMessage());
            var_dump($this->db->query("SHOW WARNINGS")->fetch());
            return false;
        }
        return true;    
    }

    public function setRecoveryToken($id, $token)
    {
        $sql=<<<SQL
UPDATE cuenta
SET recuperar=:token,
    recuperar_hasta=(NOW() + INTERVAL 30 MINUTE)
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':token', $token);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->rowCount();
    }

    public function getIdByRecoveryToken($token)
    {
        $sql=<<<SQL
SELECT id
FROM cuenta
WHERE recuperar=:token
    AND recuperar_hasta > NOW()
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':token', $token);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    public function resetPassword($user, $token, $password, $salt)
    {
        $sql=<<<SQL
SELECT id
FROM cuenta
WHERE usuario=:usuario
    AND recuperar=:token
    AND recuperar_hasta >= NOW()
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':usuario', $user);
        $statement->bindParam(':token', $token);
        $statement->execute();
        $id = $statement->fetch(\PDO::FETCH_COLUMN);

        $sql=<<<SQL
UPDATE cuenta
SET contrasenia=:contrasenia,
    sal=:sal,
    recuperar=NULL,
    recuperar_hasta=NULL
WHERE usuario=:usuario
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':contrasenia', $password);
        $statement->bindParam(':sal', $salt);
        $statement->bindParam(':usuario', $user);
        $statement->execute();

        return $statement->rowCount();
    }

    public function changePassword($user, $password, $salt)
    {
        $sql=<<<SQL
UPDATE cuenta
SET contrasenia=:contrasenia,
    sal=:sal,
    recuperar=NULL,
    recuperar_hasta=NULL
WHERE usuario=:usuario
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':contrasenia', $password);
        $statement->bindParam(':sal', $salt);
        $statement->bindParam(':usuario', $user);
        $statement->execute();

        return $statement->rowCount();
    }

    public function accountHasPermission($accountId, $permission)
    {
        $sql=<<<SQL
SELECT permiso
FROM rol_permiso rp, cuenta_rol cr
WHERE cr.rol = rp.rol
    AND cr.cuenta =:id
    AND (permiso =:permiso
        OR permiso IN (SELECT aceptable FROM permiso_permiso WHERE requerido =:permiso))
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $accountId);
        $statement->bindParam(':permiso', $permission);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function roleHasPermission($rolId, $permission)
    {
        $sql=<<<SQL
SELECT permiso
FROM rol_permiso rp
WHERE rol =:rol
    AND (permiso =:permiso
        OR permiso IN (SELECT aceptable FROM permiso_permiso WHERE requerido =:permiso))
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':rol', $rolId);
        $statement->bindParam(':permiso', $permission);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }
    
    public function search($q)
    {
        $q = '%'.$q.'%';
        $sql=<<<SQL
SELECT c.usuario as 'usuario', c.id as 'id', e.nombre as 'nombrep'
FROM cuenta c, empleado e
WHERE usuario LIKE :q
    AND c.id = e.id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':q', $q);
        $statement->execute();

        $usuario = $statement->fetchAll(\PDO::FETCH_ASSOC);

$sql=<<<SQL
SELECT r.nombre
FROM cuenta_rol cr, rol r
WHERE cr.cuenta=:id
    AND cr.rol = r.id
SQL;
        $statement = $this->db->prepare($sql);

        foreach($usuario as &$user){
            $statement->bindParam(':id', $user['id']);
            $statement->execute();
            $user['rol'] = $statement->fetchAll(\PDO::FETCH_ASSOC);
        }

        return $usuario;
    }

    /**
     * Comprueba si el id especificado corresponde a un permiso en la base de datos
     *
     * @param int $id id del permiso a buscar
     * @return bool
     */
    public function exists(int $id)
    {
        $sql =<<<SQL
SELECT COUNT(1)
FROM cuenta
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN) != 0;
    }
}
