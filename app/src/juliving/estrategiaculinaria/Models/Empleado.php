<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Empleado{

    private $db;

    public function __construct(){
        $this->db = DataBase::getInstance()->getConnection();
    }

    public function count()
    {
        $sql=<<<SQL
SELECT COUNT(1)
FROM empleado
SQL;
        $statement = $this->db->prepare($sql);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_COLUMN);
    }

    public function getByPage($page)
    {
        $offset = ($page - 1) * 5;
        $sql=<<<SQL
SELECT *
FROM empleado
ORDER BY nombre
LIMIT 5 OFFSET :offset
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindValue(':offset', $offset, \PDO::PARAM_INT);
        
        $statement->execute();
        return [
            'result' => $statement->fetchAll(\PDO::FETCH_ASSOC),
            'count' => $this->count()
        ];
    }

    public function getAll(){
        $sql=<<<SQL
SELECT *
FROM empleado 
WHERE empleado.id NOT IN (SELECT id 
                        FROM cuenta)
SQL;
        $statement = $this->db->prepare($sql);
        //$statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function getById($id){
        $sql=<<<SQL
SELECT *
FROM empleado
WHERE id=:id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function search($q){
        $q = '%'.$q.'%';
        $sql=<<<SQL
SELECT *
FROM empleado
WHERE nombre LIKE :q
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':q', $q);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function searchAutoComplete($q){
        $q = '%'.$q.'%';
        $sql=<<<SQL
SELECT id as data, nombre as value
FROM empleado
WHERE nombre LIKE :q
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':q', $q);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getEAndCById($id){
        $sql=<<<SQL
SELECT c.usuario, e.nombre, c.id
FROM empleado e, cuenta c
WHERE c.id=:id AND c.id = e.id
SQL;
        $statement = $this->db->prepare($sql);
        $statement->bindParam(':id', $id);
        $statement->execute();

        return $statement->fetch(\PDO::FETCH_ASSOC);
    }    
}
