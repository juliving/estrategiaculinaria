<?php

namespace Juliving\EstrategiaCulinaria\Models;

class Puesto{

    private $db;

    public function __construct(){
        $this->db = DataBase::getInstance()->getConnection();
    }


    public function getAll(){
        $sql=<<<SQL
SELECT CONCAT(nombre, CONCAT(' - $', salario)) AS nombre, id
FROM puesto
ORDER BY nombre
SQL;
        $statement = $this->db->prepare($sql);
        
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }
}