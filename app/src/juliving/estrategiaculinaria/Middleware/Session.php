<?php

namespace Juliving\EstrategiaCulinaria\Middleware;

use Slim\Http\Request;
use Slim\Http\Response;
use Juliving\EstrategiaCulinaria\Models as Models;
use Interop\Container\ContainerInterface as ContainerInterface;
/**
 * Middleware utilizado para el manejo del control de acceso basado en roles
 */
class Session
{

    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view = $container->get('renderer');
    }
    /**
     * 1. Obtener los permisos asociados al nombre de cierta ruta
     * 2. Validar que el usuario actual tenga al menos un permiso de los associados y guardar en 
     *    la variable request un arreglo con los permisos que si tiene
     * 
     * Valida si existe una sesión válida activa.
     * Si existe, comprueba que el actor tenga el permiso de realizar la acción que solicitó.
     * Si no, comprueba si la acción no necesita ningún permiso especial (Comprobando que el rol por defecto lo tenga concedido)
     *
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response Respuesta después de procesar middleware y controlador
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $route = $request->getAttribute('route');

        if (is_null($route)) {
            return $next($request, $response);
        }
        $permission = $route->getName();
        

        $cuentas = new Models\Cuenta();
        
        if (isset($_SESSION['id'])) {
            $hasPermission = $cuentas->accountHasPermission($_SESSION['id'], $permission);
            if (!$hasPermission) {
                // TODO: Mostrar vista de error de permisos
                return $this->view->render($response->withStatus(403), 'errores/403.phtml', [
                    'header' => [
                        'page_title' => 'No tiene permisos pare realizar la acción solicitada'
                    ],
                    'footer' => []
                ]);
            }
            
            return $next($request->withAttribute('permisos', $hasPermission), $response);
        } else {
            $hasPermission = $cuentas->roleHasPermission(Models\Cuenta::DEFAULT_ROLE, $permission);
            if (!$hasPermission && $permission !== '/login') {
                return $response->withStatus(302)->withHeader('Location', '/login?url='.$request->getUri()->getPath());
            } else if (!$hasPermission) {
                // TODO: Mostrar vista de error de permisos
                return $response->withJson(['permisos' => "No tienes permisos"]);
            } else{
                return $next($request, $response);
            }
        }
    }
}
