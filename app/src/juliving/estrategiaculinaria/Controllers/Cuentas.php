<?php

namespace Juliving\EstrategiaCulinaria\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface as ContainerInterface;
use Juliving\EstrategiaCulinaria\Models as Models;
use Mailgun\Mailgun;

class Cuentas
{
    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view = $container->get('renderer');
    }

    public function index(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Cuentas",
            ],
            'footer' => [

            ],
            'nameSingular' => "cuenta",
            'namePlural' => "cuentas",
            'addUrl' => "/cuentas/registrar",
            'searchUrl' => "/cuentas/buscar",
            'listUrl' => "/cuentas/listar",
            'adminRol' => "/cuentas/administrar",
        ];
        return $this->view->render($response, 'sectionC.phtml', $data);
    }

    public function showLoginForm(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Iniciar sesión",
                'styles' => ["/css/login.css"],
            ],
            'footer' => [],
        ];

        $_Rr = $request->getQueryParam('recoveryRequest');
        $Rr = is_null($_Rr) ? '' : $_Rr;
        $data['recover'] = $Rr;

        $_R = $request->getQueryParam('recovery');
        $R = is_null($_R) ? '' : $_R;
        $data['recovered'] = $R;

        $_U = $request->getQueryParam('userNotFound');
        $U = is_null($_U) ? '' : $_U;
        $data['userNotFound'] = $U;

        $_F = $request->getQueryParam('failed');
        $F = is_null($_F) ? '' : $_F;
        $data['failed'] = $F;

        return $this->view->render($response, 'cuentas/login.phtml', $data);
    }

    public function login(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody(); //POST
        $_url = $request->getQueryParam('url'); //GET
        $_url = ltrim($_url, '/');
        $url = is_null($_url) ? '' : $_url;

        $cuentas = new Models\Cuenta();
        $cuenta = $cuentas->getByUser($data['usuario']);

        if (!$cuenta) {
            return $response->withStatus(302)->withHeader('Location', '/login?failed=1&url='.$url);
        }

        /*$response->getBody()->write(\var_export($data));
        $response->getBody()->write(\var_export($cuenta));
        $response->getBody()->write(hash('sha256', $data['contrasenia'].$cuenta['sal']));

        return $response;*/

        if ($cuenta['contrasenia'] !== hash('sha256', $data['contrasenia'].$cuenta['sal'])) {
            return $response->withStatus(302)->withHeader('Location', '/login?failed=1&url='.$url);
        }

        $_SESSION['usuario'] = $cuenta['usuario'];
        $_SESSION['id'] = $cuenta['id'];

        return $response->withStatus(302)->withHeader('Location', '/'.$url);
    }

    /**
     * Termina la sesión actual vaciando el arreglo de sesión y destruyéndola
     * Redirige al usuario a la página de inicio de sesión
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function logout(Request $request, Response $response, array $args)
    {
        session_unset(); //Desmontar la sesión
        session_destroy(); //Eliminar la sesión
        return $response->withStatus(302)->withHeader('Location', '/login');
    }

    public function showRecoveryRequestForm(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Recuperar cuenta",
            ],
            'footer' => [],
        ];
        return $this->view->render($response, 'cuentas/recuperar.phtml', $data);
    }

    /**
     * Genera y almacena un token de recuperación de cuenta para el usuario especificado
     * Envía un correo electrónico a la dirección registrada a la persona a la que corresponde la cuenta
     * Y redirige al usuario a la página de inicio de sesión
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function recoverRequest(Request $request, Response $response, array $args)
    {
        $ApiMKey = \getenv('MAILGUN_API_KEY');
        $mgClient = new Mailgun($ApiMKey);
        $domain = \getenv('MAILGUN_API_DOMAIN');
        $user = $request->getParsedBody()['usuario'];
        $cuentas = new Models\Cuenta();
        $persona = $cuentas->getIdByUser($user);
        if (!$persona) {
            return $response->withStatus(302)->withHeader('Location', '/login?recoveryRequest=1');
        }
        $correo = (new Models\Persona())->getEMailById($persona);
        if ($persona) {
            $token = bin2hex(random_bytes(32));
            $cuentas->setRecoveryToken($persona, $token);
        }
        $data = [
            'header' => [
                'page_title' => "Recuperar cuenta",
            ],
            'footer' => [],
        ];

        $usuario = (new Models\Persona())->getNameById($persona);

        //'https://test.estrategiaculinaria.com'
        $enlace = $request->getUri()->getBaseUrl().$request->getUri()->getPath().'/'.$token;

        $mensaje = 'Estimado/a usuario,

        Muchas gracias por utilizar el servicio de recuperación de cuentas

        A continuación te otorgamos un link para que puedas reestrablecer tu contraseña: '. $enlace. '

        Recuerda que el enlace caducará luego de 30 min y deberás iniciar el proceso nuevamente.
        ';

        $result = $mgClient->sendMessage($domain, array(
            'from'    => 'Estrategia Culinaria | Cuentas <cuentas@estrategiaculinaria.com>',
            'to'      => "$usuario<$correo>",
            'subject' => 'Solicitud de restablecimiento de contraseña',
            'text'    => $mensaje
        ));
        return $response->withStatus(302)->withHeader('Location', '/login?recoveryRequest=1');
    }

    public function showRecoveryForm(Request $request, Response $response, $args)
    {
        $cuentas = new Models\Cuenta();
        $id = $cuentas->getIdByRecoveryToken($args['token']);

        if (!$id) {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }
        $data = [
            'header' => [
                'page_title' => "Recuperar cuenta",
            ],
            'footer' => [
                'scripts' => ['/js/reestablecer.js'],
            ],
        ];
        return $this->view->render($response, 'cuentas/recuperando.phtml', $data);
    }

    public function recover(Request $request, Response $response, $args)
    {
        $user = $request->getParsedBody()['usuario'];
        $password = $request->getParsedBody()['contrasenia'];

        $salt = bin2hex(random_bytes(4));
        $password = hash('sha256', $password.$salt);


        $token = $args['token'];

        $cuentas = new Models\Cuenta();

        $result = $cuentas->resetPassword($user, $token, $password, $salt);
        if ($result === 0) {
            //throw new \Slim\Exception\NotFoundException($request, $response);
            return $response->withStatus(302)->withHeader('Location', '/login?userNotFound=1');
        }
        return $response->withStatus(302)->withHeader('Location', '/login?recovery=1');
    }

    public function showAccountInfo(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Información de cuenta",
                'styles' =>['/css/prueba.css'], 
            ],
            'footer' => [
                'scripts' => ['/js/reestablecer.js'],
            ],
        ];

        $data['usuario'] = $_SESSION['usuario'];

        $_PS = $request->getQueryParam('success');
        $PS = is_null($_PS) ? '' : $_PS;
        $data['success'] = $PS;

        $_PF = $request->getQueryParam('fail');
        $PF = is_null($_PF) ? '' : $_PF;
        $data['fail'] = $PF;

        return $this->view->render($response, 'cuentas/cuenta.phtml', $data);
    }

    public function changePassword(Request $request, Response $response, $args)
    {
        //Obtengo el nombre de usuario activo usando a variable de sessión definida en función de login
        $user = $_SESSION['usuario'];
        //Obtengo la contraseña actual proporcinada por el usuario usando getParsedBody que es POST
        $passwordact = $request->getParsedBody()['contraseniaact'];
        //Obtengo la contraseña nueva proporcinada por el usuario usando getParsedBody que es POST
        $passwordnew = $request->getParsedBody()['contrasenia'];

        //Creo un modelo de cuenta
        $cuentas = new Models\Cuenta();

        //Pido a la base de datos la sal y contraseña actual utilizando el nombre de usuario
        $sesion = $cuentas->getByUser($user);

        //Concateno la contraseña y la sal y genero la función de hash
        $passwordact = hash('sha256', $passwordact.$sesion['sal']);;
        
        //Comparar las contraseñas, si no coinciden mandar una excepción/error
        if($passwordact !== $sesion['contrasenia'])
            throw new \Slim\Exception\NotFoundException($request, $response);

        //Si coinciden, continuar con la función

        $data = [
            'header' => [
                'page_title' => "Información de cuenta",
            ],
            'footer' => [],
        ];

        //Generar una sal aleatoria y concatenarsela a la nueva contraseña 
        $salt = bin2hex(random_bytes(4));
        $passwordnew = hash('sha256', $passwordnew.$salt);
        
        //Registar la nueva contraseña en la base de datos
        $result = $cuentas->changePassword($user, $passwordnew, $salt);

        if ($result === 0) {
            //throw new \Slim\Exception\NotFoundException($request, $response);
            return $response->withStatus(302)->withHeader('Location', '/cuentas/cuenta?fail=1');
        }
        return $response->withStatus(302)->withHeader('Location', '/cuentas/cuenta?success=1');

        return $this->view->render($response, 'cuentas/cuenta.phtml', $data);
    }

    public function showAddForm(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Registrar cuenta",
                'styles' => ['/css/prueba.css'], 
            ],
            'footer' => [
                'scripts' => ['/js/reestablecer.js', '/js/jquery.autocomplete.min.js'],
            ],
        ];

        $data['cuentas'] = (new Models\Empleado)->getAll();
        $data['roles'] = (new Models\Rol)->getAll();

        // var_dump($data['roles']);
        // die();
        return $this->view->render($response, 'cuentas/registrar.phtml', $data);
    }

    public function add(Request $request, Response $response, $args){
        $cuentas = new Models\Cuenta();
        //Regresa el contenido del formulario
        $cuenta = $request->getParsedBody();
        $password = $cuenta['contrasenia'];

        $salt = bin2hex(random_bytes(4));
        $password = hash('sha256', $password.$salt);

        $id = $cuentas->create($cuenta, $password, $salt);

        if($id){
            // El registro fue exitoso
            return $response->withStatus(302)->withHeader('Location', '/empleados/detalles/'.$id.'?successrc=1');
        }else{
            $response->getBody()->write("Ocurrio un error");
        }
        return $response;
    }

    public function search(Request $request, Response $response, $args){
        if(isset($args['page']))
            $page = $args['page'];
        else
            $page = 1;
        
        $data = [
            'header' => [
                'page_title' => "Cuentas",
            ],
            'nameSingular' => 'cuenta',
            'searchUrl' => '/cuentas/buscar',
        ];
        $cuentas = (new Models\Cuenta())->search($request->getQueryParam('q_cuenta'));
        $data['cuentas'] = $cuentas;

        if(!$cuentas){
            $cuentas = [];
        }
        
        $cuenta = (new Models\Cuenta ())->getByPageE($page);

        //TODO: Entender a la función de get query param e implementar una consulta que me regrese los usuarios y la página

        $data['pages'] = ceil($cuenta['count'] / 5.0);
        $data['currentPage'] = $page;

        return $this->view->render($response, 'cuentas/listar.phtml', $data);
    }

    public function showEditForm(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Editar",
                'styles' => ['/css/prueba.css'], 
            ],
            'footer' => [
                'scripts' => ['/js/forms.js', '/js/jquery.autocomplete.min.js', '/js/reestablecer.js'],
            ],
        ];

        $data['cuentase'] = (new Models\Empleado)->getEAndCById($args['id']);
        $data['roles'] = (new Models\Rol)->getAll();
        $data['cuentaRol'] = (new Models\Rol)->getAllCR($args['id']);

        return $this->view->render($response, 'cuentas/registrar.phtml', $data);
    }

    public function edit(Request $request, Response $response, $args)
    {
        $cuentas = new Models\Cuenta();
        //Devuelve el arreglo _POST
        $cuenta = $request->getParsedBody();

        $password = $cuenta['contrasenia'];

        $salt = bin2hex(random_bytes(4));
        $password = hash('sha256', $password.$salt);

        foreach ($cuenta as $key => &$value) {
            if ($value == '') {
                $value = null;
            }
        }

        $result = $cuentas->update($cuenta, $password, $salt);
        
        if ($result) {
            return $response->withStatus(302)->withHeader('Location', '/empleados/detalles/'.$cuenta['id'].'?successe=1');
        }
    }

    public function list(Request $request, Response $response, $args){
        if(isset($args['page']))
            $page = $args['page'];
        else
            $page = 1;
        
        $data = [
            'header' => [
                'page_title' => "Cuentas",
            ],
            'nameSingular' => "cuenta",
            'searchUrl' => "/cuentas/buscar",
        ];
        $cuentas = (new Models\Cuenta ())->getByPage($page);

        $data['cuentas'] = $cuentas['result'];
        $data['pages'] = ceil($cuentas['count'] / 5.0);
        $data['currentPage'] = $page;
        $S = $request->getQueryParam('success') == 1 ? 'La cuenta fue eliminada exitosamente' : null;
        $data['correcto'] = $S;
        
        return $this->view->render($response, 'cuentas/listar.phtml', $data);
    }

    public function manage(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Administrar",
            ],
            'footer' => [
                'scripts' => ['/js/cuentas_ajax.js'],
            ],
        ];

        $S = $request->getQueryParam('success') == 1 ? 'Los permisos fueron actualizados exitosamente' : null;
        $data['correcto'] = $S;

        $data['roles'] = (new Models\Cuenta ())->getAllRol();
        $data['permisos'] = (new Models\Cuenta ())->getAllPermiso();
        
        return $this->view->render($response, 'cuentas/administrando.phtml', $data);
    }

    public function updatePermits(Request $request, Response $response, $args){
        //Cuando no hay nada en la base de datos manda error
        $permisos = new Models\Cuenta();
        //Regresa el contenido del formulario
        $permiso = $request->getParsedBody();
        foreach ($permisos as &$permiso) {
            if ($permiso == '') {
                $permiso = null;
            }
        }
        // var_dump($permiso);
        // die();
  
        $result = $permisos->updatePermits($permiso);
        if ($result) {
            return $response->withStatus(302)->withHeader('Location', '/cuentas/administrar?success=1');
        }
    }

    public function rolAjax(Request $request, Response $response, $args){
        $roles = new Models\Cuenta();

        $results = $roles->searchActual($args['rol']);

        if(!$results){
            $results = array();
        }
        return $response->withJson($results);
    }
}
