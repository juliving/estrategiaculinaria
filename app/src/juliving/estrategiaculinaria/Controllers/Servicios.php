<?php

namespace Juliving\EstrategiaCulinaria\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface as ContainerInterface;
use Juliving\EstrategiaCulinaria\Models as Models;

class Servicios
{
    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->view = $container->get('renderer');
    }

    public function index(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Servicios",
            ],
            'footer' => [

            ],
            'nameSingular' => "servicio",
            'namePlural' => "servicios",
            'addUrl' => "/servicios/registrar",
            'searchUrl' => "/servicios/buscar",
            'listUrl' => "/servicios/listar",
            'reportsUrl' => "/personas/reportes",
        ];
        return $this->view->render($response, 'section.phtml', $data);
    }

    public function add(Request $request, Response $response, $args){
        //Cuando no hay nada en la base de datos manda error
        $servicios = new Models\Servicio();
        //Regresa el contenido del formulario
        $servicio = $request->getParsedBody();

        //Asigna Null en lugar de "" a los campos del formulario
        foreach ($servicio as $key => &$value) {            
            if($value == '')
                $value = null;
            
        }

        foreach ($servicio['actividades'] as $key => &$value) {
            if (!(isset($value['prioridad']))) {
                // Si no hay número, desechar la fila
                unset($servicio['actividades'][$key]);
            }
        }

        //Si no hay presentación desechar proveedores
        foreach ($servicio['proveedores'] as $key => &$value) {
            if ($value['proveedor'] == "") {
                // Si no hay número, desechar la fila
                unset($servicio['proveedores'][$key]);
            }
        }

        //Si no hay unidad desechar insumos
        foreach ($servicio['insumos'] as $key => &$value) {
            if ($value['insumo'] == "") {
                // Si no hay número, desechar la fila
                unset($servicio['insumos'][$key]);
            }
        }

        //var_dump($servicio);
        //die();
        $id = $servicios->create($servicio);

        if($id){
            // El registro fue exitoso
            return $response->withStatus(302)->withHeader('Location', '/servicios/detalles/'.$id.'?successr=1');
        }else{
            $response->getBody()->write("Ocurrio un error");
        }
        return $response;
    }

    public function detail(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Servicio",
            ],
            'footer' => [
                'scripts' => ['/js/eliminar_servicio.js'],
            ],
        ];

        $servicio = (new Models\Servicio())->getById($args['id']);

        if($servicio === false){
            //return $response->withStatus(404, 'Not Found');
            //return $response->withHeader('Location', '/');
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

        $insumos = (new Models\Insumo())->getByServicio($args['id']);
        $proveedores = (new Models\Proveedor())->getByServicio($args['id']);
        $actividades = (new Models\Actividad())->getByServicio($args['id']);
        $data['servicio'] = $servicio;
        $data['insumos'] = $insumos;
        $data['proveedores'] = $proveedores;
        $data['actividades'] = $actividades;
        
        $S = null;

        if($request->getQueryParam('successr') == 1){
            $S = 'El servicio fue agregado exitosamente';
        }
        if($request->getQueryParam('successe') == 1){
            $S = 'El servicio fue actualizado exitosamente';
        }
        $data['correcto'] = $S;

        return $this->view->render($response, 'servicios/detalles.phtml', $data);
    }

    public function erase(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "No se pudo realizar acción",
            ],
            'footer' => [
                'scripts' => [],
            ],
            'titulo' => "No se ha podido eliminar el servicio solicitado",
            'mensaje' => "Compruebe que no existan eventos o servicios enlazados a la persona",
        ];

        $id = $request->getQueryParam('id');

        $success = (new Models\Servicio())->eraseById($id);

        if($success === 1){
            // La eliminación fue exitosa
            return $response->withStatus(302)->withHeader('Location', '/servicios/listar?success=1');
        }else{
            return $this->view->render($response, 'errores/general.phtml', $data);
        }
    }

    public function list(Request $request, Response $response, $args){
        if(isset($args['page']))
            $page = $args['page'];
        else
            $page = 1;
        
        $data = [
            'header' => [
                'page_title' => "Servicios",
            ],
            'nameSingular' => "servicio",
            'searchUrl' => "/servicios/buscar",
        ];
        $servicios = (new Models\Servicio())->getByPage($page);
        $data['servicios'] = $servicios['result'];
        $data['pages'] = ceil($servicios['count'] / 5.0); //2
        $data['currentPage'] = $page; //1

        $S = $request->getQueryParam('success') == 1 ? 'El servicio fue eliminado exitosamente' : null;
        $data['correcto'] = $S;

        return $this->view->render($response, 'servicios/listar.phtml', $data);
    }

    public function search(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Servicios",
            ],
            'nameSingular' => 'servicio',
            'searchUrl' => '/servicios/buscar',
        ];
        $servicios = (new Models\Servicio())->search($request->getQueryParam('q_servicio'));
        if(!$servicios){
            $servicios = [];
        }
        $data['servicios'] = $servicios;
        return $this->view->render($response, 'servicios/listar.phtml', $data);
    }

    public function showAddForm(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Registrar servicio",
            ],
            'footer' => [
                'scripts' => ['/js/forms.js', '/js/jquery.autocomplete.min.js', '/js/servicios_registrar.js'],
            ],

        ];

        $data['unidades'] = (new Models\Unidad)->getAll();

        return $this->view->render($response, 'servicios/registrar.phtml', $data);
    }


    public function showEditForm(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Editar",
            ],
            'footer' => [
                'scripts' => ['/js/forms.js', '/js/jquery.autocomplete.min.js', '/js/servicios_registrar.js', '/js/eliminar_servicio.js'],
            ],
        ];

        $data['servicio'] = (new Models\Servicio())->getById($args['id']);
        $data['insumos'] = (new Models\Insumo())->getByServicio($data['servicio']['id']);
        $data['proveedores'] = (new Models\Proveedor())->getByServicio($data['servicio']['id']);
        $data['actividades'] = (new Models\Actividad())->getByServicio($data['servicio']['id']);


        //var_dump($data['proveedores'][0]['nombre']);
        //die();
        //var_dump($data['actividades'][0]['subactividades'][0]['nombre']);
        //die();

        return $this->view->render($response, 'servicios/registrar.phtml', $data);
    }


    public function edit(Request $request, Response $response, $args)
    {
        $servicios = new Models\Servicio();
        //Devuelve el arreglo _POST
        $servicio = $request->getParsedBody();
        foreach ($servicio as $key => &$value) {
            if ($value == '') {
                $value = null;
            }
        }

        foreach ($servicio['insumos'] as $key => &$value) {
            if (($value['insumo']) === '') {
                // Si no hay id de proveedor, desechar la fila
                unset($servicio['insumos'][$key]);
            } else {
                if ($value['cantidad'] === '') {
                    // TODO: regresar a edición de formulario
                } else {
                    foreach ($value as $k => &$v) {
                        if ($v == '') {
                            $v = null;
                        }
                    }
                }
            }
        }

        foreach ($servicio['proveedores'] as $key => &$value) {
            if (($value['proveedor']) === '') {
                // Si no hay id de proveedor, desechar la fila
                unset($servicio['proveedores'][$key]);
            } else {
                if ($value['costo'] === '') {
                    // TODO: regresar a edición de formulario
                } else {
                    foreach ($value as $k => &$v) {
                        if ($v == '') {
                            $v = null;
                        }
                    }
                }
            }
        }

        foreach ($servicio['actividades'] as $key => &$value) {
            if (($value['nombre']) === '') {
                // Si no hay nombre de actividad, desechar la fila
                unset($servicio['actividades'][$key]);
            } else {
                if ($value['descripcion'] === '' || $value['prioridad'] === '') {
                    // TODO: regresar a edición de formulario
                } else {
                    foreach ($value as $k => &$v) {
                        if ($v == '') {
                            $v = null;
                        }
                    }
                }
            }
        }

        $result = $servicios->update($servicio);
        if ($result) {
            return $response->withStatus(302)->withHeader('Location', '/servicios/detalles/'.$servicio['id'].'?successe=1');
        }

    }


    public function searchAjax(Request $request, Response $response, $args){
        $servicios = new Models\Servicio();
        $results = $servicios->searchAutoComplete($request->getQueryParam('q_servicios'));
        if(!$results)
            $results = array();
        $result['suggestions'] = $results;
        $response->getBody()->write(\json_encode($result));
        return $response;
    }

}
