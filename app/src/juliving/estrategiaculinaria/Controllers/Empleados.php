<?php

namespace Juliving\EstrategiaCulinaria\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface as ContainerInterface;
use Juliving\EstrategiaCulinaria\Models as Models;

class Empleados
{
    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view = $container->get('renderer');
    }

    public function index(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Empleados",
            ],
            'footer' => [

            ],
            'nameSingular' => "empleado",
            'namePlural' => "empleados",
            'addUrl' => "/personas/registrar?tipo=empleado",
            'searchUrl' => "/empleados/buscar",
            'listUrl' => "/empleados/listar",
            'reportsUrl' => "/empleados/reportes",
        ];
        return $this->view->render($response, 'section.phtml', $data);
    }

    public function detail(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Empleado",
            ],
            'footer' => [
                'scripts' => ['/js/eliminar_persona.js'],
            ],
        ];

        $persona = (new Models\Persona())->getById($args['id']);
        $cuenta = (new Models\Cuenta())->getById($args['id']);
        $tipos = [];
        $tiposRutas = Personas::getRoutesByType();
        foreach($persona['tipo'] as $tipo){
            $tipos[$tipo] = $tiposRutas[$tipo].'/detalles/'.$persona['id'];
        }
        
        $telefonos = (new Models\Telefono())->getByPerson($args['id']);
        $eventos = (new Models\Evento())->getByEmpleado($args['id']);

        if ($persona === false) {
            //return $response->withStatus(404, 'Not Found');
            //return $response->withHeader('Location', '/');
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

        $data['cuenta'] = $cuenta;
        $data['persona'] = $persona;
        $data['domicilio'] = $persona['domicilio'];
        unset($persona['domicilio']);
        $data['telefonos'] = $telefonos;
        
        $data['eventos'] = $eventos;
        $data['tipos'] = $tipos;

        $S = null;
        
        if($request->getQueryParam('successrc') == 1){
            $S = 'La cuenta fue agregada exitosamente';
        }
        if($request->getQueryParam('successr') == 1){
            $S = 'La persona fue agregada exitosamente';
        }
        if($request->getQueryParam('successe') == 1){
            $S = 'La persona fue actualizada exitosamente';
        }
        $data['correcto'] = $S;

        return $this->view->render($response, 'empleados/detalles.phtml', $data);
    }

    public function list(Request $request, Response $response, $args)
    {
        if(isset($args['page']))
            $page = $args['page'];
        else
            $page = 1;

        $data = [
            'header' => [
                'page_title' => "Empleados",
            ],
            'nameSingular' => 'empleado',
            'searchUrl' => '/empleados/buscar',
            'tipo' => 'empleado',
            'views' => 'empleados',
        ];
        $proveedores = (new Models\Empleado())->getByPage($page);
        $data['personas'] = $proveedores['result'];
        $data['pages'] = ceil($proveedores['count'] / 5.0);
        $data['currentPage'] = $page;
        $S = $request->getQueryParam('success') == 1 ? 'El empleado fue eliminado exitosamente' : null;
        $data['correcto'] = $S;
        return $this->view->render($response, 'personas/listar.phtml', $data);
    }

     public function search(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Empleados",
            ],
            'nameSingular' => 'empleado',
            'searchUrl' => '/empleados/buscar',
            'tipo' => 'empleado',
            'views' => 'empleados',
        ];
        $personas = (new Models\Empleado())->search($request->getQueryParam('q_empleado'));
        if(!$personas){
            $personas = [];
        }
        $data['personas'] = $personas;
        return $this->view->render($response, 'personas/listar.phtml', $data);
    }

    public function showAddForm(Request $request, Response $response, $args){
        $args['tipo'] = 'empleado';
        return (new Personas($this->container))->showAddForm($request, $response, $args);
    }

    public function add(Request $request, Response $response, $args){
        $args['path'] = '/empleados';
        return (new Personas($this->container))->add($request, $response, $args);
    }

    public function searchAjax(Request $request, Response $response, $args)
    {
        $empleados = new Models\Empleado();
        $results = $empleados->searchAutoComplete($request->getQueryParam('q_empleados'));
        if (!$results) {
            $results = array();
        }
        $result['suggestions'] = $results;
        $response->getBody()->write(\json_encode($result));
        return $response;
    }
}
