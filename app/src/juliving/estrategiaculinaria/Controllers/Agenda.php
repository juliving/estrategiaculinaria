<?php

namespace Juliving\EstrategiaCulinaria\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface as ContainerInterface;
use Juliving\EstrategiaCulinaria\Models as Models;

class Agenda
{
    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view = $container->get('renderer');
    }

    public function index(Request $request, Response $response, array $args)
    {
        $data = [
            'header' => [
                'page_title' => 'Agenda',
                'styles' => [
                    '/css/fullcalendar.min.css'
                ],
            ],
            'footer' => [
                'scripts' => [
                    "/js/fullcalendar/lib/moment.min.js",
                    "/js/fullcalendar/fullcalendar.js",
                    "/js/calendar.js",
                    "/js/fullcalendar/locale/es.js",
                ],
            ],
        ];
        return $this->view->render($response, 'agenda/index.phtml', $data);
    }


    /**
     * Obtiene los eventos dentro del rango de fecha especificado
     * como parametros GET y los devuelve en un json
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function sendEvents(Request $request, Response $response, array $args)
    {
        $start = $request->getQueryParam('start');
        $end = $request->getQueryParam('end');
        $eventos = (new Models\Evento())->getByDateRange($start, $end);
        $data = [];
        foreach($eventos as $evento){
            $data[] = [
                'id' => $evento['id'],
                'title' => $evento['nombre'],
                'start' => $evento['fecha'],
                'color' => Eventos::getColorByStatus($evento['estado']),
                //'end' => "2017-11-${i}T$i:00:00",
                'description' => $evento['descripcion'],
            ];
        }

        return $response->withJson($data);
    }
}
