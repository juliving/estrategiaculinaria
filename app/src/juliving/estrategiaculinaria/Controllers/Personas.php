<?php

namespace Juliving\EstrategiaCulinaria\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface as ContainerInterface;
use Juliving\EstrategiaCulinaria\Models as Models;

class Personas
{
    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view = $container->get('renderer');
    }

    public function erase(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "No se pudo realizar acción",
            ],
            'footer' => [
                'scripts' => [],
            ],
            'titulo' => "No se ha podido eliminar la persona solicitada",
            'mensaje' => "Compruebe que no existan eventos o servicios enlazados a la persona",
        ];

        $id = $request->getQueryParam('id');

        $success = (new Models\Persona())->eraseById($id);

        if($success){
            // La eliminación fue exitosa
            return $response->withStatus(302)->withHeader('Location', '/personas/listar?success=1');
        }else{
            return $this->view->render($response, 'errores/general.phtml', $data);
        }
    }

    public function showAddForm(Request $request, Response $response, $args)
    {
        $tipo = $request->getQueryParam('tipo');
        if (!isset(self::getRoutesByType()[$tipo])) {
            return $response->withStatus(404);
        }
        $data = [
            'header' => [
                'page_title' => "Registrar persona",
            ],
            'footer' => [
                'scripts' => ['/js/forms.js', '/js/personas_registrar.js', '/js/geocode.js'],
            ],
            'tipo' => $tipo,
        ];
        return $this->view->render($response, 'personas/registrar.phtml', $data);
    }

    public function add(Request $request, Response $response, $args)
    {
        $personas = new Models\Persona();
        //Devuelve el arreglo _POST
        $persona = $request->getParsedBody();

        foreach ($persona as $key => &$value) {
            if ($value == '') {
                $value = null;
            }
        }
        foreach ($persona['telefonos'] as $key => &$value) {
            if (($value['numero']) === '') {
                // Si no hay número, desechar la fila
                unset($persona[$key]);
            } else {
                if ($value['tipo'] === '') {
                    // TODO: regresar a edición de formulario
                } else {
                    foreach ($value as $k => &$v) {
                        if ($v == '') {
                            $v = null;
                        }
                    }
                }
            }
        }
        /*
        foreach ($persona['telefonos'] as $telefono) {
            foreach ($telefono as $key => $value) {
                if($key === 'numero' && strlen($value) === 0){
                    unset($persona)
                }
            }
            if($key == '')
                $value = null;
        }
        */

        $id = $personas->create($persona);
        if ($id) {
            // El registro fue exitoso
            return $response->withStatus(302)->withHeader('Location', self::getRoutesByType()[$persona['tipo'][0]].'/detalles/'.$id.'?successr=1);
        } else {
            $response->getBody()->write("Ocurrio un error");
        }
        return $response;
    }
    /**
     * @deprecated Cada controlador debe de implementar la vista de la persona correspondiente
     */
    public function detail(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Registrar persona",
            ],
            'footer' => [
                'scripts' => ['/js/eliminar_persona.js'],
            ],
        ];

        $persona = (new Models\Persona())->getById($args['id']);

        if ($persona === false) {
            //return $response->withStatus(404, 'Not Found');
            //return $response->withHeader('Location', '/');
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

        $data['persona'] = $persona;

        return $this->view->render($response, 'personas/registrar.phtml', $data);
    }

    public function showEditForm(Request $request, Response $response, $args)
    {
        $tipo = $request->getQueryParam('tipo');
        if (!isset(self::getRoutesByType()[$tipo])) {
            return $response->withStatus(404);
        }
        $data = [
            'header' => [
                'page_title' => "Editar",
            ],
            'footer' => [
                'scripts' => ['/js/forms.js', '/js/personas_registrar.js', '/js/geocode.js', '/js/eliminar_persona.js'],
            ],
        ];
        $data['persona'] = (new Models\Persona())->getById($args['id']);
        $data['telefonos'] = (new Models\Telefono())->getByPerson($data['persona']['id']);
        $data['domicilio'] = $data['persona']['domicilio'];
        unset($data['persona']['domicilio']);
        return $this->view->render($response, 'personas/registrar.phtml', $data);
    }

    public static function getRoutesByType()
    {
        return [
            'cliente' => '/clientes',
            'empleado' => '/empleados',
            'proveedor' => '/proveedores',
        ];
    }

    public function edit(Request $request, Response $response, $args)
    {
        $tipo = $request->getQueryParam('tipo');
        if (!isset(self::getRoutesByType()[$tipo])) {
            return $response->withStatus(404);
        }
        $personas = new Models\Persona();
        //Devuelve el arreglo _POST
        $persona = $request->getParsedBody();
        foreach ($persona as $key => &$value) {
            if ($value == '') {
                $value = null;
            }
        }
        foreach ($persona['direccion'] as $key => &$value) {
            if($value === '')
                $value = null;
        }
        foreach ($persona['telefonos'] as $key => &$value) {
            if (($value['numero']) === '') {
                // Si no hay número, desechar la fila
                unset($persona['telefonos'][$key]);
            } else {
                if ($value['tipo'] === '') {
                    // TODO: regresar a edición de formulario
                } else {
                    foreach ($value as $k => &$v) {
                        if ($v == '') {
                            $v = null;
                        }
                    }
                }
            }
        }
        $result = $personas->update($persona);
        if ($result) {
            return $response->withStatus(302)->withHeader('Location', self::getRoutesByType()[$tipo] . '/detalles/' . $persona['id'].'?successe=1');
        }
    }

    /**
     * @deprecated Cada controlador debe de implementar la vista de las personas correspondientes
     */
    public function list(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => htmlentities($request->getQueryParam('tipo')),
            ],
            'nameSingular' => htmlentities($request->getQueryParam('tipo')),
            'personas' => [
                [
                    'id' => 1,
                    'rfc' => null,
                    'nombre' => "Ivett Nunez",
                    'correo' => "ivett@gmail.com",
                ],
                [
                    'id' => 3,
                    'rfc' => "COGL9704278T8",
                    'nombre' => "Lino Contreras",
                    'correo' => null,
                ],
            ],
            'tipo' => $request->getQueryParam('tipo'),
        ];
        return $this->view->render($response, 'personas/listar.phtml', $data);
    }
}
