<?php

namespace Juliving\EstrategiaCulinaria\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface as ContainerInterface;
use Juliving\EstrategiaCulinaria\Models as Models;

class Eventos
{
    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->view = $container->get('renderer');
    }

    public function index(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Eventos",
            ],
            'footer' => [
                'scripts' => ['/js/jquery.autocomplete.min.js'],
            ],
            'nameSingular' => "evento",
            'namePlural' => "eventos",
            'addUrl' => "/eventos/registrar",
            'searchUrl' => "/eventos/buscar",
            'listUrl' => "/eventos/listar",
            'reportsUrl' => "/eventos/reportes",
        ];
        return $this->view->render($response, 'section.phtml', $data);
    }

    public function showAddForm(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Registrar Evento",
            ],
            'footer' => [
                'scripts' => ['/js/jquery.autocomplete.min.js', '/js/forms.js', '/js/eventos_registrar.js', '/js/geocode.js'],
            ],

        ];
        $data['puestos'] = (new Models\Puesto())->getAll();
        return $this->view->render($response, 'eventos/registrar.phtml', $data);
    }


    public function add(Request $request, Response $response, $args){

        $evento = $request->getParsedBody();
        $evento['fecha'] = (new \DateTime($evento['fecha'] . ' ' . $evento['hora']))->format('Y-m-d H:i:s');
        unset($evento['hora']);

        foreach($evento as &$field){
            if(is_string($field) && $field === '')
                $field = null;
        }

        foreach($evento['servicios'] as $key => $value){
            if($value['servicio'] === '')
                unset($evento['servicios'][$key]);
        }

        foreach($evento['empleados'] as $key => $value){
            if(!isset($value['puesto']) || $value['puesto'] === '' || $value['empleado'] === '')
                unset($evento['empleados'][$key]);
        }

        foreach($evento['contactos'] as $key => $value){
            if($value['nombre'] === '')
                unset($evento['contactos'][$key]);
        }
        $id = (new Models\Evento())->create($evento);
        die();
        //return $response->withJson($evento);
        $response->getBody()->write(json_encode($request->getParsedBody()));
        return $response;
    }

    public function erase(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "No se pudo realizar acción",
            ],
            'footer' => [
                'scripts' => [],
            ],
            'titulo' => "No se ha podido eliminar el evento solicitado",
            'mensaje' => "Compruebe que no existan eventos o servicios enlazados a la persona",
        ];

        $id = $request->getQueryParam('id');

        $success = (new Models\Evento())->eraseById($id);

        if($success){
            // La eliminación fue exitosa
            return $response->withStatus(302)->withHeader('Location', '/eventos/listar');
        }else{
            return $this->view->render($response, 'errores/general.phtml', $data);
        }
    }

    public function list(Request $request, Response $response, $args){
        if(isset($args['page']))
            $page = $args['page'];
        else
            $page = 1;

        $data = [
            'header' => [
                'page_title' => "Eventos",
            ],
            'nameSingular' => "evento",
            'searchUrl' => "/eventos/buscar",
        ];
        $eventos = (new Models\Evento())->getByPage($page);
        $data['eventos'] = $eventos['result'];
        $data['pages'] = ceil($eventos['count'] / 5.0);
        $data['currentPage'] = $page;
        return $this->view->render($response, 'eventos/listar.phtml', $data);
    }

    public function getEventTasksAjax(Request $request, Response $response, $args){
        $eventos = new Models\Evento();
        $tareas = new Models\Tarea();
        $clientes = new Models\Cliente();
        $evento = $eventos->getById($args['id']);
        $evento['cliente'] = $clientes->getById($evento['cliente']);
        $evento['tareas'] = $tareas->getByEvent($args['id']);
        //Esto va a devolver un arreglo con todas las tareas
        //Ahora en el js, data.tareas sera un arreglo
        $response->getBody()->write(json_encode($evento));
        return $response;
    }

    public function search(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Eventos",
            ],
            'nameSingular' => 'evento',
            'searchUrl' => '/eventos/buscar',
        ];
        $eventos = (new Models\Evento())->search($request->getQueryParam('q_evento'));
        if(!$eventos){
            $eventos = [];
        }
        $data['eventos'] = $eventos;
        return $this->view->render($response, 'eventos/listar.phtml', $data);
    }

    public function searchAjax(Request $request, Response $response, $args){
        $eventos = new Models\Evento();
        $results = $eventos->searchAutoComplete($request->getQueryParam('q_eventos'));
        if(!$results)
            $results = array();
        $result['suggestions'] = $results;
        $response->getBody()->write(\json_encode($result));
        return $response;
    }

    public static function getColorByStatus($status){
        switch ($status) {
            case 'presupuesto':
                return "green";
            case 'confirmado':
                return "#3a87ad";
            case 'cancelado':
                return "black";
            default:
                return "yellow";
        }
    }

    public function postTareaState(Request $request, Response $response, $args){
        $tarea = $request->getParsedBody();
        $result = (new Models\Evento())->updateDate($tarea);
        return $result;
    }
}
