<?php

namespace Juliving\EstrategiaCulinaria\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface as ContainerInterface;
use Juliving\EstrategiaCulinaria\Models as Models;
// reference the Dompdf namespace
use Dompdf\Dompdf;

class Insumos
{
    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->view = $container->get('renderer');
    }

    public function reporte(Request $request, Response $response, $args){
        // instantiate and use the dompdf class
        ini_set('memory_limit','2000M');
        set_time_limit(0);
        $dompdf = new Dompdf();

        //$dompdf->set_option('chroot', '/insumos');
        
        //$content = $this->view->fetch('/Prueba/reporte.html');

        //$html = file_get_contents(__DIR__ . '/../../../../vendor/dompdf/dompdf/Prueba/reporte.html');
        //$html = file_get_contents(__DIR__ . '/../Views/insumos/reporte.phtml');
        $dompdf->set_option('isHtml5ParserEnabled', true);

        //Consulta de la base de datos

        $data = new Models\Insumo();
        $insumos = $data->reporte();
        $insumos = ["insumos" => $insumos];

        $dompdf->loadhtml($this->view->fetch('insumos/reporte.phtml', $insumos));
        $dompdf->setBasePath('/../');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('letter', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $stream = fopen('php://temp','r+');
        fwrite($stream, $dompdf->output());
        rewind($stream);

        $fecha = (new \DateTime("now", new \DateTimeZone("America/Mexico_City")))->format("Y-m-d_H:m:s");
        
        return $response->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Type', 'application/octet-stream')
            ->withHeader('Content-Type', 'application/download')
            ->withHeader('Content-Description', 'File Transfer')
            ->withHeader('Content-Transfer-Encoding', 'binary')
            ->withHeader('Content-Disposition', 'attachment; filename="reporte_insumos' . $fecha . '.pdf"')
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
            ->withHeader('Pragma', 'public')
            ->withBody(new \Slim\Http\Stream($stream)); // al
    }

    public function index(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Insumos",
            ],
            'footer' => [
                'scripts' => ['/js/jquery.autocomplete.min.js'],

            ],
            'nameSingular' => "insumo",
            'namePlural' => "insumos",
            'addUrl' => "/insumos/registrar",
            'searchUrl' => "/insumos/buscar",
            'listUrl' => "/insumos/listar",
            'reportsUrl' => "/insumos/reportes",
        ];
        return $this->view->render($response, 'section.phtml', $data);
    }

    public function add(Request $request, Response $response, $args){
      //Cuando no hay nada en la base de datos manda error
      $insumos = new Models\Insumo();
      //Regresa el contenido del formulario
      $insumo = $request->getParsedBody();
      //var_dump($insumo);
      //die();
      //Asigna Null en lugar de "" a los campos del formulario
      foreach ($insumo as $key => &$value) {
          if($value == '')
              $value = null;
      }

      //llaves primarias de Insumo (y de persona)

      foreach ($insumo['proveedores'] as $key => &$value) {
            if(($value['proveedor']) === ''){
                // Si no hay id, desechar la fila
                unset($insumo[$key]);
            }else{
                if($value['presentacion'] === '' || $value['cantidad'] === ''){
                    // TODO: regresar a edición de formulario
                }else{
                    foreach ($value as $k => &$v) {
                        if($v == '')
                            $v = null;
                    }
                }
            }
        }

      $id = $insumos->create($insumo);
      //var_dump($insumo);
      //die();

      if($id){
          // El registro fue exitoso
          return $response->withStatus(302)->withHeader('Location', '/insumos/detalles/'.$id.'?successr=1');
      }else{
          $response->getBody()->write("Ocurrio un error");
      }
      return $response;
    }

    public function detail(Request $request, Response $response, $args){
        $data = [
            'header' => [
                'page_title' => "Insumo",
            ],
            'footer' => [
                'scripts' => ['/js/eliminar_servicio.js'],
            ],

        ];

        $insumo = (new Models\Insumo()) -> getById($args['id']);
        if($insumo === false){
            //return $response->withStatus(404, 'Not Found');
            //return $response->withHeader('Location', '/');
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

        $proveedores = (new Models\Proveedor())->getByInsumo($args['id']);
        
        
        $insumo['unidad'] = (new Models\Unidad)->getBySimbolo($insumo['unidad']);
        $data['insumo'] = $insumo;

        $data['proveedores'] = $proveedores;

        $S = null;
        
        if($request->getQueryParam('successr') == 1){
            $S = 'El insumo fue agregado exitosamente';
        }
        if($request->getQueryParam('successe') == 1){
            $S = 'El insumo fue actualizado exitosamente';
        }
        $data['correcto'] = $S;

        return $this->view->render($response, 'insumos/detalles.phtml', $data);
    }

    public function showEditForm(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Editar",
            ],
            'footer' => [
                'scripts' => ['/js/forms.js', '/js/jquery.autocomplete.min.js', '/js/insumos_registrar.js', '/js/eliminar_insumo.js'],
            ],
        ];

        $data['insumo'] = (new Models\Insumo())->getById($args['id']);
        $data['proveedores'] = (new Models\Proveedor())->getMinimumByInsumo($data['insumo']['id']);
        $data['unidades'] = (new Models\Unidad)->getAll();
       
        //var_dump($data['proveedores'][0]['nombre']);
        //die();
        return $this->view->render($response, 'insumos/registrar.phtml', $data);
    }


    public function edit(Request $request, Response $response, $args)
    {
        $insumos = new Models\Insumo();
        //Devuelve el arreglo _POST
        $insumo = $request->getParsedBody();
        foreach ($insumo as $key => &$value) {
            if ($value == '') {
                $value = null;
            }
        }
        foreach ($insumo['proveedores'] as $key => &$value) {
            if (($value['proveedor']) === '') {
                // Si no hay id de proveedor, desechar la fila
                unset($insumo['proveedores'][$key]);
            } else {
                if ($value['cantidad'] === '' || $value['presentacion'] === '' || $value['costo'] === '') {
                    // TODO: regresar a edición de formulario
                } else {
                    foreach ($value as $k => &$v) {
                        if ($v == '') {
                            $v = null;
                        }
                    }
                }
            }
        }

        $result = $insumos->update($insumo);
        if ($result) {
            return $response->withStatus(302)->withHeader('Location', '/insumos/detalles/'.$insumo['id'].'?successe=1');
        }
    }

    public function erase(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "No se pudo realizar acción",
            ],
            'footer' => [
                'scripts' => [],
            ],
            'titulo' => "No se ha podido eliminar el insumo solicitado",
            'mensaje' => "Compruebe que no existan eventos o servicios enlazados a la persona",
        ];

        $id = $request->getQueryParam('id');

        $success = (new Models\Insumo())->eraseById($id);

        if($success === 1){
            // La eliminación fue exitosa
            return $response->withStatus(302)->withHeader('Location', '/insumos/listar?success=1');
        }else{
            return $this->view->render($response, 'errores/general.phtml', $data);
        }
    }

    public function list(Request $request, Response $response, $args)
    {
        if(isset($args['page']))
            $page = $args['page'];
        else
            $page = 1;

        $data = [
            'header' => [
                'page_title' => "Insumos",
            ],
            'nameSingular' => "insumo",
            'searchUrl' => "/insumos/buscar",
        ];
      
        $insumos = (new Models\Insumo())->getByPage($page);
        $data['insumos'] = $insumos['result'];
        $data['pages'] = ceil($insumos['count'] / 5.0);
        $data['currentPage'] = $page;
        $S = $request->getQueryParam('success') == 1 ? 'El insumo fue eliminado exitosamente' : null;
        $data['correcto'] = $S;
        return $this->view->render($response, 'insumos/listar.phtml', $data);
    }

    public function search(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Insumos",
            ],
            'nameSingular' => 'insumo',
            'searchUrl' => '/insumos/buscar',
        ];
        $insumos = (new Models\Insumo())->search($request->getQueryParam('q_insumo'));
        if(!$insumos){
            $insumos = [];
        }
        $data['insumos'] = $insumos;
        return $this->view->render($response, 'insumos/listar.phtml', $data);
    }

    public function showAddForm(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Registrar insumo",
            ],
            'footer' => [
                'scripts' => ['/js/jquery.autocomplete.min.js', '/js/forms.js', '/js/insumos_registrar.js'],
            ],
        ];

        $data['unidades'] = (new Models\Unidad)->getAll();
        /*$data['unidades'] = ['kg' => 'Kilogramo', 'l' => 'Litro', 'm' => 'Metro', 'pqt' => 'Paquete']; */

        return $this->view->render($response, 'insumos/registrar.phtml', $data);
    }

    public function searchAjax(Request $request, Response $response, $args){
        $insumos = new Models\Insumo();

        $results = $insumos->searchAutoComplete($request->getQueryParam('q_insumos'));
        if(!$results){
            $results = array();
        }
        $result['suggestions'] = $results;
        $response->getBody()->write(\json_encode($result));
        return $response;
    }

}
