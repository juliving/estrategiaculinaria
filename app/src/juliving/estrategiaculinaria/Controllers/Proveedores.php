<?php

namespace Juliving\EstrategiaCulinaria\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface as ContainerInterface;
use Juliving\EstrategiaCulinaria\Models as Models;

class Proveedores
{
    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view = $container->get('renderer');
    }

    public function index(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Proveedores",
            ],
            'footer' => [

            ],
            'nameSingular' => "proveedor",
            'namePlural' => "proveedores",
            'addUrl' => "/personas/registrar?tipo=proveedor",
            'searchUrl' => "/proveedores/buscar",
            'listUrl' => "/proveedores/listar",
            'reportsUrl' => "/proveedores/reportes",
        ];
        return $this->view->render($response, 'section.phtml', $data);
    }

    public function detail(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Proveedor",
            ],
            'footer' => [
                'scripts' => ['/js/eliminar_persona.js'],
            ],
        ];

        $persona = (new Models\Persona())->getById($args['id']);
        $tipos = [];
        $tiposRutas = Personas::getRoutesByType();
        foreach($persona['tipo'] as $tipo){
            $tipos[$tipo] = $tiposRutas[$tipo].'/detalles/'.$persona['id'];
        }
        $telefonos = (new Models\Telefono())->getByPerson($args['id']);
        $eventos = (new Models\Evento())->getByProveedor($args['id']);

        if ($persona === false) {
            //return $response->withStatus(404, 'Not Found');
            //return $response->withHeader('Location', '/');
            throw new \Slim\Exception\NotFoundException($request, $response);
        }   
        
        $data['persona'] = $persona;
        $data['domicilio'] = $persona['domicilio'];
        unset($persona['domicilio']);
        $data['telefonos'] = $telefonos;

        $data['eventos'] = $eventos;
        $data['tipos'] = $tipos;
        
        $S = null;
        
        if($request->getQueryParam('successr') == 1){
            $S = 'La persona fue agregada exitosamente';
        }
        if($request->getQueryParam('successe') == 1){
            $S = 'La persona fue actualizada exitosamente';
        }
        $data['correcto'] = $S;

        return $this->view->render($response, 'proveedores/detalles.phtml', $data);
    }

    public function list(Request $request, Response $response, $args)
    {
        if(isset($args['page']))
            $page = $args['page'];
        else
            $page = 1;

        $data = [
            'header' => [
                'page_title' => "Proveedores",
            ],
            'nameSingular' => 'proveedor',
            'searchUrl' => '/proveedores/buscar',
            'tipo' => 'proovedor',
            'views' => 'proveedores',
        ];

        $proveedores = (new Models\Proveedor())->getByPage($page);
        $data['personas'] = $proveedores['result'];
        $data['pages'] = ceil($proveedores['count'] / 5.0);
        $data['currentPage'] = $page;
        $S = $request->getQueryParam('success') == 1 ? 'El proveedor fue eliminado exitosamente' : null;
        $data['correcto'] = $S;

        return $this->view->render($response, 'personas/listar.phtml', $data);
    }

    public function porServicioAjax(Request $request, Response $response, $args){
        $proveedores = new Models\Proveedor();
        $results = $proveedores->getByServicioSelect($request->getQueryParam('q_servicio'));
        if (!$results) {
            $results = array();
        }
        return $response->withJson($results);
    }

    public function search(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Proveedores",
            ],
            'nameSingular' => 'proveedor',
            'searchUrl' => '/proveedor/buscar',
            'tipo' => 'proveedor',
            'views' => 'proveedores',
        ];
        $personas = (new Models\Proveedor())->search($request->getQueryParam('q_proveedor'));
        if (!$personas) {
            $personas = [];
        }
        $data['personas'] = $personas;
        return $this->view->render($response, 'personas/listar.phtml', $data);
    }

    public function showAddForm(Request $request, Response $response, $args)
    {
        $args['tipo'] = 'proveedor';
        return (new Personas($this->container))->showAddForm($request, $response, $args);
    }

    public function add(Request $request, Response $response, $args)
    {
        $args['path'] = '/proveedores';
        return (new Personas($this->container))->add($request, $response, $args);
    }

    public function searchAjax(Request $request, Response $response, $args)
    {
        $proveedores = new Models\Proveedor();
        $results = $proveedores->searchAutoComplete($request->getQueryParam('q_proveedores'));
        if (!$results) {
            $results = array();
        }
        $result['suggestions'] = $results;
        $response->getBody()->write(\json_encode($result));
        return $response;
    }
}
