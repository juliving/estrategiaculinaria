<?php

namespace Juliving\EstrategiaCulinaria\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface as ContainerInterface;
use Juliving\EstrategiaCulinaria\Models as Models;

class Clientes
{
    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->view = $container->get('renderer');
    }

    public function index(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Clientes",
            ],
            'footer' => [

            ],
            'nameSingular' => "cliente",
            'namePlural' => "clientes",
            'addUrl' => "/personas/registrar?tipo=cliente",
            'searchUrl' => "/clientes/buscar",
            'listUrl' => "/clientes/listar",
            'reportsUrl' => "/clientes/reportes",
        ];
        return $this->view->render($response, 'section.phtml', $data);
    }

    public function detail(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Cliente",
            ],
            'footer' => [
                'scripts' => ['/js/eliminar_persona.js'],
            ],
        ];

        $persona = (new Models\Persona())->getById($args['id']);

        if ($persona === false) {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

        $tipos = [];
        $tiposRutas = Personas::getRoutesByType();
        foreach($persona['tipo'] as $tipo){
            $tipos[$tipo] = $tiposRutas[$tipo].'/detalles/'.$persona['id'];
        }
        
        $telefonos = (new Models\Telefono())->getByPerson($args['id']);
        $eventos = (new Models\Evento())->getByClient($args['id']);

        if ($persona === false) {
            //return $response->withStatus(404, 'Not Found');
            //return $response->withHeader('Location', '/');
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

        $data['persona'] = $persona;
        $data['domicilio'] = $persona['domicilio'];
        unset($persona['domicilio']);
        $data['telefonos'] = $telefonos;
        
        $data['eventos'] = $eventos;
        $data['tipos'] = $tipos;   

        $S = null;

        if($request->getQueryParam('successr') == 1){
            $S = 'La persona fue agregado exitosamente';
        }
        if($request->getQueryParam('successe') == 1){
            $S = 'La persona fue actualizado exitosamente';
        }
        $data['correcto'] = $S;

        return $this->view->render($response, 'clientes/detalles.phtml', $data);
    }
    
    public function list(Request $request, Response $response, $args)
    {
        if(isset($args['page']))
            $page = $args['page'];
        else
            $page = 1;

        $data = [
            'header' => [
                'page_title' => "Clientes",
            ],
            'nameSingular' => 'cliente',
            'searchUrl' => '/clientes/buscar',
            'tipo' => 'cliente',
            'views' => 'clientes',
        ];
        $clientes = (new Models\Cliente())->getByPage($page);
        $data['personas'] = $clientes['result'];
        $data['pages'] = ceil($clientes['count'] / 5.0);
        $data['currentPage'] = $page;
        $S = $request->getQueryParam('success') == 1 ? 'El cliente fue eliminado exitosamente' : null;
        $data['correcto'] = $S;
        return $this->view->render($response, 'personas/listar.phtml', $data);
    }

    public function search(Request $request, Response $response, $args)
    {
        $data = [
            'header' => [
                'page_title' => "Clientes",
            ],
            'nameSingular' => 'cliente',
            'searchUrl' => '/clientes/buscar',
            'tipo' => 'cliente',
            'views' => 'clientes',
        ];
        $personas = (new Models\Cliente())->search($request->getQueryParam('q_cliente'));
        if (!$personas) {
            $personas = [];
        }
        $data['personas'] = $personas;
        return $this->view->render($response, 'personas/listar.phtml', $data);
    }

    public function searchAjax(Request $request, Response $response, $args)
    {
        $clientes = new Models\Cliente();
        $results = $clientes->searchAutoComplete($request->getQueryParam('q_clientes'));
        if (!$results) {
            $results = array();
        }
        $result['suggestions'] = $results;
        $response->getBody()->write(\json_encode($result));
        return $response;
    }
}
