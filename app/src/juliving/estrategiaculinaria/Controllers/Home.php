<?php

namespace Juliving\EstrategiaCulinaria\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface as ContainerInterface;

class Home
{
    protected $container;
    protected $view;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->view = $container->get('renderer');
    }

    public function home(Request $request, Response $response, $args){
        $data = [
            'permisos' => $request->getAttribute('permisos'),
            'header' => [
                'page_title' => htmlentities($request->getQueryParam('tipo')),
            ],
            'data' => [
                'usuario' => $_SESSION['usuario'],
            ],
            'footer' => [
                
            ],
        ];
        return $this->view->render($response, 'home.phtml', $data);
    }

}
