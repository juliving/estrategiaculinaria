$(document).ready(function() { 
    var checkPass = function(e){
        let contra = $('#contrasenia').val();
        errors = [];
        //$(this).attr('id', $(this).attr('id').replace(indexPattern, index));
        if(contra.length < 8)
            errors.push('* La longitud de la contraseña debe de ser de 8 caracteres');
        if(contra.length > 20)
            errors.push('* La longitud de la contraseña no debe exceder los 20 caracteres');
        if(contra.search(/[A-ZÑ]/) < 0)
            errors.push('* Tu contraseña debe contener una mayúscula');                   
        if(contra.search(/[0-9]/) < 0)
            errors.push('* Tu contraseña debe contener una número.');
        if(contra.search(/[\*!#\.@&]/) < 0)
            errors.push('* Tu contraseña debe contener un caracter especial');    
        if(!(contra.search(/\s/) < 0))
            errors.push('* Tu contraseña no puede contener espacios en blanco');    
        if(errors.length !== 0){   
            $('label[for=contrasenia]').attr('data-error', errors.join('\n'));
            $('#contrasenia').addClass('invalid'); //Pone rojo el mensaje y pone el data error
            return false;
        }else{
            $('#contrasenia').removeClass('invalid');
            $('#contrasenia').addClass('valid');
            return true;
        }
    };
    $('#contrasenia').on('input', checkPass);
    $('#contra').on('submit', function(e){
        if(!checkPass(e)){
            $($('.invalid')[0]).focus();
            e.preventDefault();
        }
    });
    var recheckPass = function(e){
        let con1 = $('#contrasenia').val();
        let con2 = $('#confirmacion').val();
        if(con1 !== con2){
            $('label[for=confirmacion]').attr('data-error', 'Las contraseñas no coinciden');
            $('#confirmacion').addClass('invalid'); //Pone rojo el mensaje y pone el data error
            return false;
        }else{
            $('#confirmacion').removeClass('invalid');
            $('#confirmacion').addClass('valid');
            return true;
        }
    };
    $('#confirmacion').on('input', recheckPass);
    $('#contra').on('submit', function(e){
        if(!recheckPass(e)){
            $($('.invalid')[0]).focus();
            e.preventDefault();
        }
    });
});