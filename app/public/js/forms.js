
var cloneInput = function (parent, clase, remove) {

    var newElem = $($('.' + clase)[0]).clone();

    newElem.find('input, textarea').each(function (index) { //Buscar todos los text area e input y eliminar el texto que tenga el padre
        $(this).val(null);
        $(this).attr('disabled', null);
    });
    //$(this).find('subactividad:not(subactividad:first-child)').remove();
    $('#' + parent).prepend(newElem);

    normalizeInput(parent, clase, remove);
};

var cloneInputAct = function (parent, clase, son, sons, remove, removes) {

    var newElem = $($('.' + clase)[0]).clone(true);

    newElem.find('input, textarea').each(function (index) { //Buscar todos los text area e input y eliminar el texto que tenga el padre
        $(this).val(null);
    });

    var len = newElem.find('.'+sons).length;

    newElem.find('.'+sons).each(function (index) { //Buscar todos los text area e input y eliminar el texto que tenga el padre
        if(index < (len - 1))
            $(this).remove();
    });
    
    $('#' + parent).prepend(newElem); //Que al papá le nazca un nuevo hijo

    normalizeInputAct(parent, clase, son, sons, remove, removes);
};

var cloneInputSub = function (add, parent, son, remove) {

    var newElem = $($('.' + son)[0]).clone(true);

    newElem.find('input, textarea').each(function (index) { //Buscar todos los text area e input y eliminar el texto que tenga el padre
        $(this).val(null);
    });
    //$(this).find('subactividad:not(subactividad:first-child)').remove();
    $(add).parent().next('.' + parent).prepend(newElem);

    normalizeInputSubAdd(add, parent, son, remove);
};

var removeInput = function (el, parent, clase, remove) {
    $(el).parentsUntil('#' + parent).remove();
    normalizeInput(parent, clase, remove);
};

var removeInputSub = function (el, parent, son, remove) {
    let num = $(el).parentsUntil('.' + parent).parent().find('.' + son).length;
    $rem.splice(0, 1);
    $normal = $(el).parentsUntil('.' + parent).parent().find('.' + son);
    $(el).parentsUntil('.' + parent).remove();
    normalizeInputSubDel(num, $rem, $normal, parent, son);
};

var normalizeInput = function (parent, clase, remove) {
    var num = $('.' + clase).length;

    $('.' + remove).each(function (index) {
        $(this).attr('disabled', num > 1 ? null : '');
    });

    var indexPattern = /\d+/;
    $('#' + parent).find('.' + clase).each(function (index) {
        $(this).find('[name^=' + parent + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(indexPattern, index));
        });
        $(this).find('*[id^=' + clase + ']').each(function (i) {
            $(this).attr('id', $(this).attr('id').replace(indexPattern, index));
        });
    });
}

normalizeInputAct = function (parent, clase, son, sons, remove, removes) {
    //Cuantos elementos que tienen la clase de la variable clase hay
    var num = $('.' + clase).length;
    //Verificar si hay uno o más elementos. Si hay uno deshabilita el botón de borrar, si no, lo habilita y por default se pone de color rojo
    $('.' + remove).each(function (index) {
        $(this).attr('disabled', num > 1 ? null : '');
    });

    $('.'+removes).first().attr('disabled', '');

    //Expresión regular que permite identificar números en un texto
    var indexPattern = /\d+/;
    //Buscar todos los elementos hijo del padre cuya clase sea la de la variable clase y a cada uno de ellos hacer lo siguiente:
    $('#' + parent).find('.' + clase).each(function (index) {
        //Buscar dentro de los elementos de la clase los inputs que tengan el nombre del padre y a cada uno de ellos modificarles el número que contengan 
        //en el nombre del input por el numero del index de la iteración
        //Para normalizar al los elementos de la actividad:
        $(this).find('input[name^=' + parent + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(indexPattern, index));
        });
        $(this).find('*[id^=' + clase + ']').each(function (i) {
            $(this).attr('id', $(this).attr('id').replace(indexPattern, index));
        });
        $(this).find('textarea[name^=' + parent + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(indexPattern, index));
        });
        $(this).find('select[name^=' + parent + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(indexPattern, index));
        });
        //Para normalizar a los elementos de la subactividad
        //Modificar el número del botón de add
        $(this).find('*[id^=' + son + ']').each(function (i) {
            $(this).attr('id', $(this).attr('id').replace(indexPattern, index));
        });
        //Modifica el valor de los arreglos (primer cero) de la subactividad
        $(this).find('input[name^=' + son + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(indexPattern, index));
        });
        //Normaliza el nuevo elemento de subactividad
        $(this).find('*[id^=' + sons + ']').each(function (i) {
            $(this).attr('id', $(this).attr('id').replace(indexPattern, index));
        });
    });
}
/**
 * 
 * @param {*} add Botòn que origino el evento
 * @param {*} parent Clase padre de los elementos a normalizar
 * @param {*} son Clase de los elementos a normalizar
 * @param {*} remove Clase del botòn de eliminar elemento
 */
var normalizeInputSubDel = function (num, rem, normal, parent, son) {
    //Funciona para add y delete
    num = num - 1;

    //rem es el botón de eliminar subactividad
    rem.each(function (index) {
        $(this).attr('disabled', num > 1 ? null : '');
    });

    var indexPattern = /\d+/;
    normal.each(function (index) {
        $(this).find('input[name^=' + parent + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(indexPattern, index));
        });
        $(this).find('*[id^=' + son + ']').each(function (i) {
            $(this).attr('id', $(this).attr('id').replace(indexPattern, index));
        });
        $(this).find('textarea[name^=' + son + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(indexPattern, index));
        });
    });
}

var normalizeInputSubAdd = function (add, parent, son, remove) {
    var num;
    num = $(add).parent().next('.' + parent).find('.' + son).length;

    //TODO: Checar esta normalización, porque si hay dos actividades y una tiene dos subactividades, y borras una de ellas, la que queda se queda en rojo
    $(add).parent().next('.' + parent).find('.' + remove).each(function (index) {
        if(index < num)
            $(this).attr('disabled', num > 1 ? null : '');
    });

    var indexPattern = /\d+/;
    var sonPattern = /(.*\d+)(.*)(\d+)(.*)/;
    $(add).parent().parent().find('.' + son).each(function (index) {
        $(this).find('input[name^=' + parent + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(indexPattern, index));
        });
        $(this).find('input[name*=' + son + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(sonPattern, '$1$2' + index + '$4'));
        });
        $(this).find('*[id^=' + son + ']').each(function (i) {
            $(this).attr('id', $(this).attr('id').replace(indexPattern, index));
        });
        $(this).find('textarea[name*=' + son + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(sonPattern, '$1$2' + index + '$4'));
        });
        $(this).find('select[name*=' + son + ']').each(function (i) {
            $(this).attr('name', $(this).attr('name').replace(sonPattern, '$1$2' + index + '$4'));
        });
    });
}
