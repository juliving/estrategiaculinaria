$(document).ready(function () {
    $("#add_servicio").click(function () {
        $('#servicios select').material_select('destroy');
        cloneInput('servicios', 'servicio', 'remove_servicio');
        $('#servicios div:first-child select option:not([disabled])').remove();
        $('#servicios select').material_select();
        bindAutocompleteServicio();
        bindActualizarValor('.servicio_precio', '.servicio_cantidad');
        $('.remove_servicio').click(function () {
            removeInput(this, 'servicios', 'servicio', 'remove_servicio');
        });
    });    

    $("#add_empleado").click(function () {
        $('#empleados select').material_select('destroy');
        cloneInput('empleados', 'empleado', 'remove_empleado');
        $('#empleados select').material_select();
        bindAutocompleteEmpleado();
        bindActualizarValor('.empelado_salario', '.empelado_turnos');
        bindSelectPuesto();
        $('.remove_empleado').click(function () {
            removeInput(this, 'empleados', 'empleado', 'remove_empleado');
        });
    });    

    $("#add_contacto").click(function () {
        cloneInput('contactos', 'contacto', 'remove_contacto');
        bindAutocompleteContacto();
        $('.remove_contacto').click(function () {
            removeInput(this, 'contactos', 'contacto', 'remove_contacto');
        });
    });
    

    $('#cliente').devbridgeAutocomplete({
        serviceUrl: '/clientes/searchAjax',
        paramName: 'q_clientes',
        onSearchStart: function(params){
            $(this).removeClass('invalid');
            $('#clienteLoader').show();
        },
        onSearchComplete: function(query, suggestion){
            $('#clienteLoader').hide();
        },
        onSelect: function (suggestion) {
            $('[name=cliente]').val(suggestion.data);
            console.log('You selected: ' + suggestion.value + ', ' + suggestion.data);
            $(this).attr('disabled', 'disabled');
            $(this).removeClass('invalid');
        }
    });

    var bindActualizarValor = function(valor, cantidad){
        $(valor).on('change', function(ev){
            $(this).data('previo', $(this).val());
        });
        $(cantidad).on('change', function(ev){
            let valorEl = $(this).parent().next().find('input');
            let cantidadPrevia = $(this).data('previo');
            let valorPrevio = valorEl.data('previo');
            valorEl.val( Math.round(100 * $(this).val() * (valorPrevio / cantidadPrevia)) / 100);
            $(this).data('previo', $(this).val());
            valorEl.data('previo', valorEl.val());
        });
        
    };
  
    var bindAutocompleteServicio = function () {
        $('.autocomplete_servicio:not([disabled])').each(function(i){
            $(this).devbridgeAutocomplete({
                serviceUrl: '/servicios/searchAjax',
                paramName: 'q_servicios',
                onSelect: function (suggestion) {
                    $(this).prev().val(suggestion.data);
                    $(this).attr('disabled', 'disabled');
                    console.warn($(this).parent().parent().find('input[id*=cantidad]'));
                    console.warn($(this).parent().parent().find('input[id*=precio]'));
                    let cantidad = $(this).parent().parent().find('input[id*=cantidad]');
                    let precio = $(this).parent().parent().find('input[id*=precio]');
                    cantidad.val(1);
                    cantidad.data('previo', 1);
                    precio.val(suggestion.precio);
                    precio.data('previo', suggestion.precio);
                    Materialize.updateTextFields();
                    var input = this;
                    $.get('/proveedores/porServicioAjax', {q_servicio: suggestion.data}, function(res){
                        if(res.lenght == 0){
                            alert('No se encontraron proveedores registrados para este servicio');
                        }else{
                            var select = $(input).parent().parent().find('select');
                            select.material_select('destroy');
                            console.log(select);
                            console.log(res);
                            res.forEach(function(item, i){
                                select.append($('<option>', {
                                    value: item.id,
                                    text : item.nombre
                                }));
                            });
                            select.material_select();
                        }
                        
                    });
                    console.log('You selected: ' + suggestion.value + ', ' + suggestion.data);
                }
            });
        });
    }

    var bindAutocompleteEmpleado = function () {
        $('.autocomplete_empleado:not([disabled])').devbridgeAutocomplete({
            serviceUrl: '/empleados/searchAjax',
            paramName: 'q_empleados',
            onSelect: function (suggestion) {
                $(this).prev().val(suggestion.data);
                $(this).attr('disabled', 'disabled');
            }
        });
    }

    var bindAutocompleteContacto = function () {
        $('.autocomplete_contacto').devbridgeAutocomplete({
            serviceUrl: '/contactos/searchAjax',
            paramName: 'q_contactos',
            onSelect: function (suggestion) {
                console.log('You selected: ' + suggestion.value + ', ' + suggestion.data);
            }
        });
    }

    var bindSelectPuesto = function(){
        $('select.empleado_select').off('change').on('change', function(ev){
            console.warn('Evento EJECUTADO');
            console.error($(this));
            let turnos = $(this).parent().parent().next().next().find('input');
            let salario = $(this).parent().parent().next().next().next().find('input');

            let pattern = /.* - \$(.*)$/;
            let valor = $(this).find(":selected").text();
            salarioValue = pattern.exec(valor)[1];
            
            turnos.val(1);
            salario.val(salarioValue);
            turnos.data('previo', 1);
            salario.data('previo', salarioValue);
            Materialize.updateTextFields();
        });
    }

    bindAutocompleteContacto();
    bindAutocompleteEmpleado();
    bindAutocompleteServicio();
    bindActualizarValor('.servicio_precio', '.servicio_cantidad');
    bindActualizarValor('.empelado_salario', '.empelado_turnos');
    bindSelectPuesto();
});
