$(document).ready(function() {
    $("#add_insumo").click(function(){
        cloneInput('insumos', 'insumo', 'remove_insumo');
        bindAutocompleteInsumo();
        $('.remove_insumo').click(function(){
            removeInput(this, 'insumos', 'insumo', 'remove_insumo');
        });
    });
    

    $("#add_proveedor").click(function(){
      cloneInput('proveedores', 'proveedor', 'remove_proveedor');
      bindAutocompleteProveedor();
      $('.remove_proveedor').click(function(){
          removeInput(this, 'proveedores', 'proveedor', 'remove_proveedor');
        });
    });

    $("#add_actividad").click(function(){
        $('select').material_select('destroy');
        cloneInputAct('actividades', 'actividad', 'subactividades', 'subactividad', 'remove_actividad', 'remove_subactividad');
        $('select').material_select();
        $('.remove_actividad').click(function(){
            removeInput(this, 'actividades', 'actividad', 'remove_actividad');
        });
    });
    
    
    $(".add_subactividad").click(function(){
        $('select').material_select('destroy');
        cloneInputSub(this, 'subactividades', 'subactividad', 'remove_subactividad');
        $('select').material_select();
        $('.remove_subactividad').click(function(){
            removeInputSub(this, 'subactividades', 'subactividad', 'remove_subactividad');
        });
    });
    

    var bindAutocompleteInsumo = function(){
      $('.insumo_autocomplete').devbridgeAutocomplete({
          serviceUrl: '/insumos/searchAjax',
          paramName: 'q_insumos',
          onSelect: function (suggestion) {
              $(this).parent().find('input[type="hidden"]').val(suggestion.data);
              $(this).attr('disabled', 'disabled');
          }
      });
    }

    var bindAutocompleteProveedor = function(){
      $('.proveedor_autocomplete').devbridgeAutocomplete({
          serviceUrl: '/proveedores/searchAjax',
          paramName: 'q_proveedores',
          onSelect: function (suggestion) {
              $(this).parent().find('input[type="hidden"]').val(suggestion.data);
              $(this).attr('disabled', 'disabled');
          }
      });
    }

    bindAutocompleteInsumo();
    bindAutocompleteProveedor();
});
