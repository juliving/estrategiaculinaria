$(document).ready(function() {
    $('#rol').change(function(){
      $(".permiso").show();
      $("input[type=checkbox]").attr("checked", null);
      $.get('/cuentas/rolesAjax/' + $('#rol').val(), function(evento){
        evento.forEach(function(item, i){
          $("input[value='" + CSS.escape(item) + "']").attr("checked", "checked");
        });
      });
    });
  
});