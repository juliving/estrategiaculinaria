$(document).ready(function() { 
    $('#modal1').modal();
      // page is now ready, initialize the calendar...

    $('#agenda').fullCalendar({
        events: '/agenda/sendevents',
        defaultView: 'listMonth',
        header: { left: false, center: false, right: false},
        eventClick: function(calEvent, jsEvent, view) {
            requestData(calEvent.id);
        },
    });

    $('#calendar').fullCalendar({
        events: '/agenda/sendevents',
        header: { left: 'title', center: false},
        dayClick: function(date, jsEvent, view) {
            $('#agenda').fullCalendar( 'gotoDate', date )
        },
        eventClick: function(calEvent, jsEvent, view) {
            requestData(calEvent.id);
        },
    });


    var requestData = function(idEvent){
        $(".evento").hide();
        $(".loader").show();
        $("#modal1").modal('open');


        $.get('/eventos/getEventTasksAjax/' + idEvent, function(evento){
            $('#nombre').text(evento.nombre);
            $('#descripcion').text(evento.descripcion);
            $('#cliente').text(evento.cliente.nombre);
            $('#tareas').empty();

            /*evento.tareas = [
                                {id: '1',
                                nombre: 'Tarea A', 
                                descripcion:'Descripcion de tarea A',
                                prioridad:'alta', subtareas:[
                                                                {id:'2',
                                                                nombre: 'Tarea B',
                                                                descripcion:'Descripcion de tarea B',
                                                                prioridad:'urgente', subtareas:[]}
                                                            ],},     
                            ];*/

            if(evento.tareas === false){
                $('#tareas').text('No se encontraron tareas relacionadas a este evento');
            }
            else{
                evento.tareas.forEach(function(item, i){
                    li = $('<li>');

                    ic = $('<i>', {
                        text: 'radio_button_unchecked',
                        class: 'material-icons'
                    });
                    ic.data('id', item.id);

                    header = $('<div>', {
                        text: item.nombre,
                        class: 'collapsible-header'

                    });
                    
                    body = $('<div>', {
                        text: item.descripcion,
                        class: 'collapsible-body'
                    });
                    
                    
                    if(item.completada != null){
                        console.log("completada");
                        //header.prepend(icCchecked);
                        ic.text('check_circle');
                    }

                    header.prepend(ic);
                    li.append(header);

                    ic.on('click',function(event){
                        event.stopPropagation();
                        completed= $(this).text() === 'check_circle';
                        if (!completed){
                            $(this).text('check_circle');

                        } else {
                            $(this).text('radio_button_unchecked');
                            
                        }
                        completed = !completed;
                        //POST
                        $.post('/eventos/postTareaState', {id: $(this).data('id'), completada: completed}, function(tarea){
                            if(completed){
                                Materialize.toast('Tarea completada', 1500);
                            }
                            else{
                                Materialize.toast('Tarea pendiente', 1500);
                            }
                        });
                    });                                               

                    if(item.subtareas.length > 0 ){
                        console.log("Hay subtareas");
                        //item tiene hijos
                        lu = $('<ul>', {class: 'collapsible'});

                        item.subtareas.forEach(function(otem, i){
                            //otem es cada subtarea

                            liNested = $('<li>');
                            
                            bodyNested = $('<div>', {
                                text: otem.descripcion,
                                class: 'collapsible-body'
                            });

                            headerNested = $('<div>', {
                                text: otem.nombre,
                                class: 'collapsible-header'
                            });

                            icSub = $('<i>', {
                                text: 'check_box_outline_blank',
                                class: 'material-icons'
                            });
                            icSub.data('id', otem.id);

                            
                            if(otem.completada != null){
                                icSub.text('check_box');
                            }

                            headerNested.prepend(icSub);
                            liNested.append(headerNested);

                            icSub.on('click',function(event){
                                event.stopPropagation();
                                completed= $(this).text() === 'check_box';
                                if (!completed){
                                    $(this).text('check_box');

                                } else {
                                    $(this).text('check_box_outline_blank');
                                }
                                completed = !completed;
                                //POST
                                $.post('/eventos/postTareaState', {id: $(this).data('id'), completada: completed}, function(tarea){
                                    if(completed){
                                        Materialize.toast('Subtarea completada', 1500);
                                    }
                                    else{
                                        Materialize.toast('Subtarea pendiente', 1500);
                                    }
                                });
                            });

                            liNested.append(bodyNested);
                            lu.append(liNested);
                        });
                        body.append(lu);
                        lu.collapsible();
                    
                    } //Cierra subtareas


                    li.append(body);                   
                    $('#tareas').append(li);
                    console.log(item);

                }); //Termina el forEach
                
            $(".loader").hide();
            $(".evento").show();
            }
            
            //document.getElementById('nombre').append(document.createTextNode(data.nombre));
            }, "json");    
    };
  

});