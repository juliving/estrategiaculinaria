$(document).ready(function() {


    $("#add_proveedor").click(function(){
        cloneInput('proveedores', 'proveedor', 'remove_proveedor');
        bindAutocomplete();
        $('.remove_proveedor').click(function(){
            removeInput(this,'proveedores', 'proveedor', 'remove_proveedor')
        });
    });
    

    var bindAutocomplete = function(){
        $('.proveedor_autocomplete').devbridgeAutocomplete({
            serviceUrl: '/proveedores/searchAjax',
            paramName: 'q_proveedores',
            onSelect: function (suggestion) {
                $(this).parent().find('input[type="hidden"]').val(suggestion.data);
            }
        });
    }

    bindAutocomplete();

});
