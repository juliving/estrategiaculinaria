
var marker = null;

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: { lat: 20.6122835, lng: -100.4802583 }
  });

  var location = {lat : Number($('#lat').val()), lng : Number($('#lon').val())}
  if(location.lat !== NaN && location.lat !== 0 || location.lat != location.lng){
    marker = new google.maps.Marker({
      map: map,
      position: location
    });
    map.setCenter(location);
    map.setZoom(20);
  }

  map.addListener('click', function(e) {
    placeMarker(e.latLng, map);
  });

  function placeMarker(position, map) {
      if (marker !== null)
        marker.setMap(null);
      marker = new google.maps.Marker({
          position: position,
          map: map
      });
      map.panTo(position);
  }
  
  
  var geocoder = new google.maps.Geocoder();

  document.getElementById('remove_address').addEventListener('click', function () {
    if (marker !== null)
      marker.setMap(null);
      $('#lat').val('');
      $('#lon').val('');
  });

  document.getElementById('find_address').addEventListener('click', function () {
    geocodeAddress(geocoder, map);
  });
}

function geocodeAddress(geocoder, resultsMap) {
  let direccion = $.makeArray($('#direcciones input'));
  direccion = direccion.reduce(function (acum, elem) {
    return acum + ' ' + elem.value;
  }, '');
  geocoder.geocode({ 'address': direccion }, function (results, status) {
    if (status === 'OK') {
      resultsMap.panTo(results[0].geometry.location);
      resultsMap.setZoom(20);
      $('#lat').val(results[0].geometry.location.lat());
      $('#lon').val(results[0].geometry.location.lng());
      if (marker !== null)
        marker.setMap(null);
      marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location
      });
    } else {
      alert('No se ha podido encontrar la dirección.\n' + 'Por favor, si desea guardar las coordenadas navegue y haga click en la ubicación deseada \n' + status);
    }
  });
}
